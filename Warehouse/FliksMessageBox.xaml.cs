﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for FlicksMessageBox.xaml
    /// </summary>
    public partial class FliksMessageBox : Window
    {
        string question="";
        string type = "YesNo";
        public FliksMessageBox(string questionA, string typeA)
        {
            AddHandler(Keyboard.KeyDownEvent, (KeyEventHandler)quickKey);

            question = questionA;
            type = typeA;            
        }

        private void quickKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
            else if (e.Key == Key.Return)
            {
                this.DialogResult = true;
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            Topmost = true;

            lblQuestion.Content = question;
            if (type != "YesNo")
            {
                Bcancel.Visibility = Visibility.Hidden;
                BcancelBack.Visibility = Visibility.Hidden;
            }
        }

        public bool? Answer
        {
            get { return DialogResult; }
        }

        private void Bok_Click(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Bno_Click(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = false;
        }

        private void dim(object sender, MouseEventArgs e)
        {
            ((Image)sender).Opacity = 0.7;
        }

        private void bright(object sender, MouseEventArgs e)
        {
            ((Image)sender).Opacity = 1;
        }
    }
}
