﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for newConsignee.xaml
    /// </summary>
    public partial class newConsignee : UserControl
    {
        DataTable dt;
        public newConsignee()
        {
            InitializeComponent();


            dt = new DataTable();
            dt.Columns.Add("ID").ReadOnly = true;
            dt.Columns.Add("Name").ReadOnly = true;
            dt.Columns.Add("Address").ReadOnly = true;
            dt.Columns.Add("Contact No").ReadOnly = true;
            dt.Columns.Add("SL Name").ReadOnly = true;
            dt.Columns.Add("SL Address").ReadOnly = true;

            ArrayList arr = new ArrayList();
            arr.Add("Qatar");
            arr.Add("Kuwait");
            Ccountry.ItemsSource = arr;
        }

        private void Bsearch_Click(object sender, RoutedEventArgs e)
        {
            string sql = "SELECT * from cargo where id='"+Tvalue.Text+"'";
            Mydbs.sqlSelectorAsync(Ccountry.Text, sql, populate);
        }

        private int populate(dynamic dynJson)
        {
#if prod6
            try
            {
#endif
                
                dt.Rows.Clear();
                if (dynJson != null)
                {
                    foreach (var i in dynJson)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = My.s(i.name);
                        row["ID"] = My.s(i.id);
                        row["Address"] = My.s(i.address);
                        row["Contact No"] = My.s(i.contact_number);
                        row["SL Name"] = My.s(i.sl_name);
                        row["SL Address"] = My.s(i.sl_address);

                        dt.Rows.Add(row);
                    }
                }

                table.Items.SortDescriptions.Add(new SortDescription("ID", ListSortDirection.Descending));
                table.ItemsSource = dt.DefaultView;

                
#if prod6
            }
            catch (Exception bizee) { My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this); }
#endif

            return 1;

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            My.fitToNavs(this);
        }

        private void BprintHbl_Click(object sender, RoutedEventArgs e)
        {
            String cargoId = "";
            DataRowView dataRow = (DataRowView)table.SelectedItem;
            if (dataRow == null && table.Items.Count>1)
            {
                MessageBox.Show("Please Click the Row", My.shop);
                return;
            }
            if(table.Items.Count == 1)
                dataRow = (DataRowView)table.Items[My.colID(dt, "ID")];
            
            string id = dataRow.Row.ItemArray[0].ToString();
            myprint.printForm(true, id, Ccountry.Text);
        }
    }
}
