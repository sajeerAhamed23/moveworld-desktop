﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for MyFilter.xaml
    /// </summary>
    public partial class MyFilter : UserControl
    {
        #region Label DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String Label
        {
            get { return (String)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string),
              typeof(MyFilter), new PropertyMetadata(""));

        #endregion


        public event KeyEventHandler OnEnter
        {
            add { text.AddHandler(PreviewKeyDownEvent, value); }
            remove { text.RemoveHandler(PreviewKeyDownEvent, value); }
        }

        public MyFilter()
        {
            InitializeComponent();
        }

        private void check_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                text.IsEnabled = true;
            }
            catch { }
        }

        private void check_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                text.IsEnabled = false;
                text.Text = "";
            }
            catch { }
        }

        public void setItemSource(ArrayList e)
        {
            text.ItemsSource = e;
        }

        public string getText()
        {
            return text.Text;
        }

        public void setText(string t)
        {
            text.Text = t;
        }

        public string getLabel()
        {
            return Label;
        }
    }
}
