﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for MyInfoBox.xaml
    /// </summary>
    public partial class MyInfoBox : Window
    {
        public MyInfoBox()
        {
            InitializeComponent();
            this.Show();
        }

        public MyInfoBox(string s)
        {
            InitializeComponent();
            news.Content = s;

            this.Show();
        }

        public MyInfoBox(Brush s)
        {
            InitializeComponent();
            news.Foreground = s;
            news.BorderBrush = s;
            news.Background = Brushes.White;

            this.Show();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            Topmost = true;

            Thread thread = new Thread(() =>
            {
                Thread.Sleep(3000);
                try
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() => this.Close()));
                }
                catch { }
            });
            thread.Start();
        }

        private void news_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}
