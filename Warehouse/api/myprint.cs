﻿using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Barcode;

namespace Warehouse
{
    class myprint
    {

        public static void notepadPrint(String cargo_id, String country)
        {
            dataFetching(cargo_id,country);

            String fullText = "";

            fullText += "DATE : " + job.getPickupDate() + " " + job.getPickupTime();
            fullText += "\t HBL : " + job.getKey() + "\n";
            fullText += "CARGO TYPE : " + ((job.getIs_air() == "1") ? "Air/" : "Sea/");

            try
            {
                File.WriteAllText("Prints/HBL_" + cargo_id + ".txt", fullText);
            }
            catch (Exception bidzee) { }
        }


        public static void drawingCashReceipt(XGraphics graph, String cargo_id,String country)
        {
            //get data from db
            dataFetching(cargo_id, country);
            //createBarcode(cargo_id);

            XTextFormatter tf = new XTextFormatter(graph);
            tf.Alignment = XParagraphAlignment.Left;
            XTextFormatter tf1 = new XTextFormatter(graph);
            tf1.Alignment = XParagraphAlignment.Right;
            XFont font = new XFont("Arial", 10, XFontStyle.Regular);
            XFont fontSmall = new XFont("Arial", 8, XFontStyle.Regular);

            String isAir = (job.getIs_air() == "1") ? "Air/" : "Sea/";
            String isLankanCharges = (job.getlankanCharges() == "1") ? "Paid" : "Not Paid";

            int cashNum = Int32.Parse(job.getKey());
            XRect temp = new XRect(379, 130, 200, 0);
            tf.DrawString((cashNum + 2777) + "", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(379, 143, 200, 0);
            tf.DrawString(job.getPickupTime(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(379, 156, 200, 0);
            tf.DrawString(job.getPickupDate(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(379, 168, 200, 0);
            tf.DrawString((My.user).ToUpper(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);


            if (job.getName().Length > 40)
            {
                temp = new XRect(42, 145, 100, 0);
                tf.DrawString(job.getName().Substring(0, 38), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(42, 159, 100, 0);
                tf.DrawString("-" + job.getName().Substring(38), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }
            else
            {
                temp = new XRect(42, 145, 100, 0);
                tf.DrawString(job.getName(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }

            temp = new XRect(43, 258, 200, 0);
            tf.DrawString("1", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(332, 258, 200, 0);
            tf.DrawString(isAir + job.getIs_door(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(182, 258, 200, 0);
            tf.DrawString(cargo_id, font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);//cash receipt

            temp = new XRect(455, 258, 77, 0);
            tf1.DrawString(My.f(job.getPaid()).ToString("0.00"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);//column amount



            temp = new XRect(483, 542, 200, 0);
            tf.DrawString(My.f(job.getPaid()).ToString("0.00"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);//total

            temp = new XRect(483, 554, 200, 0);
            tf.DrawString("-", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);//discount

            temp = new XRect(483, 566, 200, 0);
            tf.DrawString(My.f(job.getPaid()).ToString("0.00"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);//net amount



            temp = new XRect(131, 582, 350, 0);
            tf.DrawString((My.NumberToWords((int)Math.Ceiling(My.f(job.getPaid())))).ToUpper(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);



            temp = new XRect(240, 675, 100, 0);
            tf.DrawString("Cash", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);//cash/cheque

            temp = new XRect(240, 716, 100, 0);
            tf.DrawString(My.f(job.getAmount()).ToString("0.00"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);//being



            temp = new XRect(53, 623, 200, 0);
            tf.DrawString("Destination Charges " + isLankanCharges, font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

        }


        public static void printCashReceipt(bool shouldOpen, String cargo_id, string country)
        {
            PdfDocument pdf = new PdfDocument();
            pdf.Info.Title = "Printing";
            PdfPage pdfPage = pdf.AddPage();
            XGraphics graph = XGraphics.FromPdfPage(pdfPage);
            graph.DrawImage(XImage.FromFile(My.jpgLocation + "CashReceipt.jpg"), 0, 0, 595, 841);

            drawingCashReceipt(graph, cargo_id,country);

            //starting the process
            string time_now = DateTime.Now.ToString("HH-mm-ss");
            string fileName = cargo_id + "_CashReceipt_" + time_now + ".pdf";
            string pdfFilename = "Prints/" + fileName;

            string directory = System.IO.Path.Combine(Environment.CurrentDirectory, "Prints");
            string filePath = System.IO.Path.Combine(directory, fileName);


            pdf.Save(pdfFilename);
            if (shouldOpen == true)
                Process.Start(filePath);
        }


        public static void drawingNewFromDb(XGraphics graph, String cargo_id,String country)
        {
            //get data from db
            dataFetching(cargo_id,country);
            createBarcode(cargo_id);

            XTextFormatter tf = new XTextFormatter(graph);
            tf.Alignment = XParagraphAlignment.Left;
            XFont font = new XFont("Arial", 10, XFontStyle.Regular);
            XFont fontSmall = new XFont("Arial", 8, XFontStyle.Regular);

            String isAir = (job.getIs_air() == "1") ? "Air/" : "Sea/";

            XRect temp = new XRect(48, 71, 200, 0);
            tf.DrawString(job.getPickupDate(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(48, 86, 200, 0);
            tf.DrawString(job.getPickupTime(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(81, 98, 200, 0);
            tf.DrawString(isAir + job.getIs_door(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(115, 113, 200, 0);
            tf.DrawString(job.getIs_colombo(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);


            temp = new XRect(48, 129, 100, 0);
            tf.DrawString(cargo_id, font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            //temp = new XRect(12, 142, 100, 0);
            //tf.DrawString(job.getQid(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);


            graph.DrawImage(XImage.FromFile("BarcodeTemp\\" + cargo_id + ".png"), 12, 142);


            if (job.getName().Length > 40)
            {
                temp = new XRect(77, 228, 240, 150);
                tf.DrawString(job.getName().Substring(0, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(77, 242, 240, 150);
                tf.DrawString("-" + job.getName().Substring(39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }
            else
            {
                temp = new XRect(77, 228, 240, 0);
                tf.DrawString(job.getName(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }

            if (job.getAddress().Length > 81)
            {
                temp = new XRect(77, 256, 240, 150);
                tf.DrawString(job.getAddress().Substring(0, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(77, 270, 240, 150);
                tf.DrawString("-" + job.getAddress().Substring(39, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(77, 284, 240, 150);
                tf.DrawString("-" + job.getAddress().Substring(78), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }
            else if (job.getAddress().Length > 41)
            {
                temp = new XRect(77, 256, 240, 150);
                tf.DrawString(job.getAddress().Substring(0, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(77, 270, 240, 150);
                tf.DrawString("-" + job.getAddress().Substring(39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }
            else
            {
                temp = new XRect(77, 256, 240, 150);
                tf.DrawString(job.getAddress(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }

            temp = new XRect(77, 289, 240, 0);
            tf.DrawString(job.getQid(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(77, 303, 240, 0);
            tf.DrawString(job.getContactNum(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(77, 317, 240, 0);
            tf.DrawString(job.getPp_no(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);


            if (job.getSl_name().Length > 41)
            {
                temp = new XRect(370, 228, 240, 150);
                tf.DrawString(job.getSl_name().Substring(0, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(370, 242, 240, 150);
                tf.DrawString("-" + job.getSl_name().Substring(39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }
            else
            {
                temp = new XRect(370, 228, 240, 0);
                tf.DrawString(job.getSl_name(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }

            if (job.getSl_address().Length > 81)
            {
                temp = new XRect(370, 256, 240, 150);
                tf.DrawString(job.getSl_address().Substring(0, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(370, 270, 240, 150);
                tf.DrawString("-" + job.getSl_address().Substring(39, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(370, 284, 240, 150);
                tf.DrawString("-" + job.getSl_address().Substring(78), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }
            else if (job.getSl_address().Length > 41)
            {
                temp = new XRect(370, 256, 240, 150);
                tf.DrawString(job.getSl_address().Substring(0, 39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
                temp = new XRect(370, 270, 240, 150);
                tf.DrawString("-" + job.getSl_address().Substring(39), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }
            else
            {
                temp = new XRect(370, 256, 240, 150);
                tf.DrawString(job.getSl_address(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
            }

            temp = new XRect(370, 303, 240, 0);
            tf.DrawString(job.getSl_contact(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(370, 317, 240, 0);
            tf.DrawString(job.getSl_pp_no(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);







            temp = new XRect(97, 595, 100, 0);
            tf.DrawString(job.getTot_package(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(97, 610, 100, 0);
            tf.DrawString((My.f(job.getTot_volume())).ToString("0.000") + " Meter Cube", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(97, 625, 100, 0);
            tf.DrawString(job.getTot_weight(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);




            temp = new XRect(369, 595, 100, 0);
            tf.DrawString(My.f(job.getAmount()).ToString("0.00"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(369, 610, 100, 0);
            tf.DrawString(My.f(job.getPaid()).ToString("0.00"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);




            /*float inc = 20.5f;
            temp = new XRect(561, 350, 100, 0);
            tf.DrawString(costOf(0), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString(costOf(1), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString(costOf(2), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString(costOf(3), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString(costOf(4), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString(costOf(5), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString(costOf(6), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(561, temp.Y + inc, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);*/



            XRect temp0 = new XRect(18, 358, 50, 0);
            XRect temp1 = new XRect(56, 358, 150, 0);
            XRect temp2 = new XRect(200, 358, 100, 0);
            XRect temp3 = new XRect(308, 358, 50, 0);
            XRect temp4 = new XRect(352, 358, 50, 0);
            XRect temp5 = new XRect(407, 358, 50, 0);
            XRect temp6 = new XRect(469, 358, 50, 0);

            for (int i = 0; i < job.getPacks().Count; i++)
            {
                Packs pack = (Packs)job.getPacks()[i];
                tf.DrawString((i + 1) + "", font, XBrushes.MidnightBlue, temp0, XStringFormats.TopLeft);
                tf.DrawString(pack.getDescr(), font, XBrushes.MidnightBlue, temp1, XStringFormats.TopLeft);
                tf.DrawString(pack.getSize(), font, XBrushes.MidnightBlue, temp2, XStringFormats.TopLeft);//packageOf(i, spec)
                tf.DrawString(pack.getQty(), font, XBrushes.MidnightBlue, temp3, XStringFormats.TopLeft);
                tf.DrawString(pack.getWeight(), font, XBrushes.MidnightBlue, temp4, XStringFormats.TopLeft);
                tf.DrawString(My.f(pack.getCmb()).ToString("0.000"), font, XBrushes.MidnightBlue, temp5, XStringFormats.TopLeft);
                tf.DrawString(pack.getRemarks(), font, XBrushes.MidnightBlue, temp6, XStringFormats.TopLeft);


                temp0.Y += 14.5;
                temp1.Y += 14.5;
                temp2.Y += 14.5;
                temp3.Y += 14.5;
                temp4.Y += 14.5;
                temp5.Y += 14.5;
                temp6.Y += 14.5;
            }


            /*tf.DrawString(Tnoofpackages.Text, font, XBrushes.MidnightBlue, temp2, XStringFormats.TopLeft);
            tf.DrawString(Tvolume.Text, font, XBrushes.MidnightBlue, temp3, XStringFormats.TopLeft);
            tf.DrawString(Ttotalweight.Text, font, XBrushes.MidnightBlue, temp4, XStringFormats.TopLeft);*/

            temp = new XRect(76, 648, 350, 0);
            tf.DrawString((My.NumberToWords((int)Math.Ceiling(My.f(job.getPaid())))).ToUpper(), fontSmall, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            /*temp = new XRect(484, 641, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(484, 674, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(316, 702, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(85, 702, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(310, 723, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(491, 744, 100, 0);
            tf.DrawString("", font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

                temp = new XRect(65, 729, 100, 0);
            tf.DrawString(paid_var.ToString("0.0"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);

            temp = new XRect(204, 729, 100, 0);
            tf.DrawString((total_var - paid_var).ToString("0.0"), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);*/

            temp = new XRect(17, 767, 300, 0);
            tf.DrawString(job.getNote(), font, XBrushes.MidnightBlue, temp, XStringFormats.TopLeft);
        }

        static Job job;
        public static void dataFetching(String id,String country)
        {
            job = new Job();
            job.setKey(id);
            ArrayList packs = new ArrayList();
            String sql = "SELECT *,id as idk,(SELECT sum(quantity) FROM package where cargo_id=idk)as tot_package,(SELECT sum(weigth) FROM package WHERE cargo_id=idk) as tot_weight,(SELECT sum(length*breadth*heigth*quantity) FROM package WHERE cargo_id=idk) as tot_volume FROM cargo WHERE id=" + job.getKey();
            dynamic dynJson = null;
            if (country == "")
                dynJson=Mydb.sqlSelector(sql);
            else
                dynJson=Mydbs.sqlSelector(country,sql);

            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    job.setActive(My.s(i.active));
                    job.setName(My.s(i.name));
                    job.setQid(My.s(i.qid));
                    job.setAddress(My.s(i.address));
                    job.setContactNum(My.s(i.contact_number));
                    try
                    {
                        job.setLang(My.s(i.location).split(":")[0]);
                        job.setLat(My.s(i.location).split(":")[1]);
                    }
                    catch (Exception ignore)
                    {
                        job.setLang("");
                        job.setLat("");
                    }


                    job.setPickerNote(My.s(i.picker_note));
                    job.setPickupDate(My.s(i.pickup_date));
                    job.setPickupTime(My.s(i.pickup_time));
                    job.setPickupedDate(My.s(i.pickedup_date));
                    job.setPickupedTime(My.s(i.pickedup_time));
                    job.setAmount(My.s(i.unit_amount));


                    job.setNote(My.s(i.note));
                    job.setOfficer_note(My.s(i.officer_note));
                    job.setAmount_spec(My.s(i.amount_spec));
                    job.setPickupedDate(My.s(i.ordered_date));
                    job.setOrdered_time(My.s(i.ordered_time));
                    job.setIs_air(My.s(i.isAir));
                    job.setIs_colombo(My.s(i.is_colombo));
                    job.setIs_door(My.s(i.is_door));
                    job.setSl_address(My.s(i.sl_address));
                    job.setSl_name(My.s(i.sl_name));
                    job.setSl_contact(My.s(i.sl_contact));
                    job.setSl_pp_no(My.s(i.sl_pp_no));
                    job.setPp_no(My.s(i.pp_no));
                    job.setPaid(My.s(i.paid));
                    job.setlankanCharges(My.s(i.lankan_charges));


                    job.setTot_weight(My.s(i.tot_weight));
                    job.setTot_package(My.s(i.tot_package));
                    job.setTot_volume(My.s(i.tot_volume));

                }
            }



            sql = "SELECT *,ifNull((length*breadth*heigth*quantity),0) as cmb FROM package WHERE cargo_id=" + job.getKey();
            dynJson = Mydbs.sqlSelector(country,sql);

            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    Packs pack = new Packs();
                    pack.setDescr(My.s(i.type));
                    pack.setSize(My.s(i.length) + "X" + My.s(i.breadth) + "X" + My.s(i.heigth));
                    pack.setCmb(My.s(i.cmb));
                    pack.setRemarks(My.s(i.remarks));
                    pack.setWeight(My.s(i.weigth));
                    pack.setQty(My.s(i.quantity));
                    packs.Add(pack);
                }
            }
            job.setPacks(packs);


        }

        public static void printForm(bool shouldOpen, String cargo_id,string country="")
        {
            PdfDocument pdf = new PdfDocument();
            pdf.Info.Title = "Printing";
            PdfPage pdfPage = pdf.AddPage();
            XGraphics graph = XGraphics.FromPdfPage(pdfPage);
            graph.DrawImage(XImage.FromFile(My.jpgLocation+country + "_Form.jpg"), 0, 0, (Double)pdfPage.Width.Point, (Double)pdfPage.Height.Point);

            drawingNewFromDb(graph, cargo_id,country);

            //starting the process
            string time_now = DateTime.Now.ToString("HH-mm-ss");
            string fileName = cargo_id + "_HBL_" + time_now + ".pdf";
            string pdfFilename = "Prints/" + fileName;

            string directory = System.IO.Path.Combine(Environment.CurrentDirectory, "Prints");
            string filePath = System.IO.Path.Combine(directory, fileName);


            pdf.Save(pdfFilename);
            if (shouldOpen == true)
                Process.Start(filePath);
        }

        public static void createBarcode(string IbillId)
        {
            //Creates fresh folder

            string folderName = "BarcodeTemp";
            System.IO.Directory.CreateDirectory(folderName);
            System.IO.DirectoryInfo di = new DirectoryInfo(folderName);
            try
            {
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }
            catch { }
            try
            {
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
            catch { }
            try
            {
                System.IO.Directory.CreateDirectory(folderName);
            }
            catch { }
            try
            {
                BarcodeDraw bdraw = BarcodeDrawFactory.GetSymbology(BarcodeSymbology.Code128);
                System.Drawing.Image barcodeImage = bdraw.Draw(My.f(IbillId).ToString("0000000000000"), 40, 2);
                barcodeImage.Save(folderName + "\\" + IbillId + ".png");
            }
            catch { }
        }

        class Job
        {
            private String qid;
            private String active, key;
            private String name, address, lat, lang, contactNum, pickupTime, pickupDate;
            private ArrayList packs;
            private String pickupedTime, pickupedDate, pickerNote, amount;
            private String tot_package, tot_volume, tot_weight;
            private String note, officer_note;
            private String amount_spec;
            private String ordered_date, ordered_time;
            private String is_air, is_colombo, is_door;
            private String sl_address, sl_name, sl_contact, sl_pp_no;
            private String pp_no;
            private String paid;
            private String lankanCharges;

            public String getlankanCharges()
            {
                return lankanCharges;
            }

            public void setlankanCharges(String lankanCharges)
            {
                this.lankanCharges = lankanCharges;
            }

            public String getQid()
            {
                return qid;
            }

            public void setQid(String qid)
            {
                this.qid = qid;
            }

            public String getNote()
            {
                return note;
            }

            public void setNote(String note)
            {
                this.note = note;
            }


            public String getOfficer_note()
            {
                return officer_note;
            }

            public void setOfficer_note(String officer_note)
            {
                this.officer_note = officer_note;
            }

            public String getAmount_spec()
            {
                return amount_spec;
            }

            public void setAmount_spec(String amount_spec)
            {
                this.amount_spec = amount_spec;
            }

            public String getOrdered_date()
            {
                return ordered_date;
            }

            public void setOrdered_date(String ordered_date)
            {
                this.ordered_date = ordered_date;
            }

            public String getOrdered_time()
            {
                return ordered_time;
            }

            public void setOrdered_time(String ordered_time)
            {
                this.ordered_time = ordered_time;
            }

            public String getIs_air()
            {
                return is_air;
            }

            public void setIs_air(String is_air)
            {
                this.is_air = is_air;
            }

            public String getIs_colombo()
            {
                return is_colombo;
            }

            public void setIs_colombo(String is_colombo)
            {
                this.is_colombo = is_colombo;
            }

            public String getIs_door()
            {
                return is_door;
            }

            public void setIs_door(String is_door)
            {
                this.is_door = is_door;
            }

            public String getSl_address()
            {
                return sl_address;
            }

            public void setSl_address(String sl_address)
            {
                this.sl_address = sl_address;
            }

            public String getSl_name()
            {
                return sl_name;
            }

            public void setSl_name(String sl_name)
            {
                this.sl_name = sl_name;
            }

            public String getSl_contact()
            {
                return sl_contact;
            }

            public void setSl_contact(String sl_contact)
            {
                this.sl_contact = sl_contact;
            }

            public String getSl_pp_no()
            {
                return sl_pp_no;
            }

            public void setSl_pp_no(String sl_pp_no)
            {
                this.sl_pp_no = sl_pp_no;
            }

            public String getPp_no()
            {
                return pp_no;
            }

            public void setPp_no(String pp_no)
            {
                this.pp_no = pp_no;
            }

            public String getPaid()
            {
                return paid;
            }

            public void setPaid(String paid)
            {
                this.paid = paid;
            }

            public String getTot_package()
            {
                return tot_package;
            }

            public void setTot_package(String tot_package)
            {
                this.tot_package = tot_package;
            }

            public String getTot_volume()
            {
                return tot_volume;
            }

            public void setTot_volume(String tot_volume)
            {
                this.tot_volume = tot_volume;
            }

            public String getTot_weight()
            {
                return tot_weight;
            }

            public void setTot_weight(String tot_weight)
            {
                this.tot_weight = tot_weight;
            }

            public Job() { }

            public String getAmount()
            {
                return amount;
            }

            public void setAmount(String amount)
            {
                this.amount = amount;
            }

            public String getLat()
            {
                return lat;
            }

            public void setLat(String lat)
            {
                this.lat = lat;
            }

            public String getLang()
            {
                return lang;
            }

            public void setLang(String lang)
            {
                this.lang = lang;
            }

            public String getContactNum()
            {
                return contactNum;
            }

            public void setContactNum(String contactNum)
            {
                this.contactNum = contactNum;
            }

            public String getPickupTime()
            {
                return pickupTime;
            }

            public void setPickupTime(String pickupTime)
            {
                this.pickupTime = pickupTime;
            }

            public String getPickupDate()
            {
                return pickupDate;
            }

            public void setPickupDate(String pickupDate)
            {
                this.pickupDate = pickupDate;
            }

            public String getActive()
            {
                return active;
            }

            public void setActive(String active)
            {
                this.active = active;
            }

            public String getPickerNote()
            {
                return pickerNote;
            }

            public void setPickerNote(String pickerNote)
            {
                this.pickerNote = pickerNote;
            }

            public String getKey()
            {
                return key;
            }

            public void setKey(String key)
            {
                this.key = key;
            }

            public String getName()
            {
                return name;
            }

            public void setName(String name)
            {
                this.name = name;
            }

            public String getAddress()
            {
                return address;
            }

            public void setAddress(String address)
            {
                this.address = address;
            }

            public String getPickupedTime()
            {
                return pickupedTime;
            }

            public void setPickupedTime(String pickupedTime)
            {
                this.pickupedTime = pickupedTime;
            }

            public String getPickupedDate()
            {
                return pickupedDate;
            }

            public void setPickupedDate(String pickupedDate)
            {
                this.pickupedDate = pickupedDate;
            }

            public ArrayList getPacks()
            {
                return packs;
            }

            public void setPacks(ArrayList packs)
            {
                this.packs = packs;
            }
        }

        class Packs
        {
            private String descr, size, qty, cmb, remarks, weight;

            public String getDescr()
            {
                return descr;
            }

            public void setDescr(String descr)
            {
                this.descr = descr;
            }

            public String getSize()
            {
                return size;
            }

            public void setSize(String size)
            {
                this.size = size;
            }

            public String getQty()
            {
                return qty;
            }

            public void setQty(String qty)
            {
                this.qty = qty;
            }

            public String getCmb()
            {
                return My.f(cmb).ToString("0.000");
            }

            public void setCmb(String cmb)
            {
                this.cmb = cmb;
            }

            public String getRemarks()
            {
                return remarks;
            }

            public void setRemarks(String remarks)
            {
                this.remarks = remarks;
            }

            public String getWeight()
            {
                return weight;
            }

            public void setWeight(String weight)
            {
                this.weight = weight;
            }
        }

    }


}
