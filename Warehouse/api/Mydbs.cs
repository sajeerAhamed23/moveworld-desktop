﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse
{
    class Mydbs
    {
        public static void sqlSelectorAsync(String country, String sql, Func<dynamic, int> populateDatagrid)
        {
            Mydb.sqlSelectorAsync(sql, populateDatagrid, My.groupServerAddress + country.ToLower() + "/api/selector.php?a=", true);
        }

        public static dynamic sqlSelector(String country, String sql)
        {
            return Mydb.sqlSelector(sql, My.groupServerAddress + country.ToLower() + "/api/selector.php?a=", true);
        }
    }
}
