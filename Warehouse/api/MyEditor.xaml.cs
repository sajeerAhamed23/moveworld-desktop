﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for MyFilter.xaml
    /// </summary>
    public partial class MyEditor : UserControl
    {
        #region Heading DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String Heading
        {
            get { return (String)GetValue(HeadingProperty); }
            set { SetValue(HeadingProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty HeadingProperty =
            DependencyProperty.Register("Heading", typeof(string),
              typeof(MyEditor), new PropertyMetadata(""));

        #endregion

        #region Color DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String Color
        {
            get { return (String)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(string),
              typeof(MyEditor), new PropertyMetadata(""));

        #endregion

        #region OtherColor DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String OtherColor
        {
            get { return (String)GetValue(OtherColorProperty); }
            set { SetValue(OtherColorProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty OtherColorProperty =
            DependencyProperty.Register("OtherColor", typeof(string),
              typeof(MyEditor), new PropertyMetadata(""));

        #endregion

        #region FileName DP

        public String FileName
        {
            get { return (String)GetValue(FileNameProperty); }
            set { SetValue(FileNameProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty FileNameProperty =
            DependencyProperty.Register("FileName", typeof(string),
              typeof(MyEditor), new PropertyMetadata(""));

        #endregion

        #region MultiLine DP

        public String MultiLine
        {
            get { return (String)GetValue(MultiLineProperty); }
            set { SetValue(MultiLineProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty MultiLineProperty =
            DependencyProperty.Register("MultiLine", typeof(string),
              typeof(MyEditor), new PropertyMetadata(""));

        #endregion

        public MyEditor()
        {
            InitializeComponent();
        }


        private void parent_Loaded(object sender, RoutedEventArgs e)
        {
            refresh();
        }

        private void refresh()
        {
            try
            {
                string fromFile = File.ReadAllText(FileName);
                text.Text = fromFile;

                news.Content = "";
            }
            catch { }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            File.WriteAllText(FileName, text.Text);
            news.Content = "Saved";
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            refresh();
        }

        private void text_TextChanged(object sender, TextChangedEventArgs e)
        {
            news.Content = "Please Save";
        }



    }
}
