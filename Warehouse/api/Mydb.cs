﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Warehouse
{
    class Mydb
    {
        public static string sqlExecutor(String sql_to_send, Boolean returnId=false, string end_sql="")
        {
            try
            {
                string bizlog = DateTime.Now.ToString("HH-mm-ss");
                File.AppendAllText(My.logFileName, bizlog + "\r\n" + sql_to_send + " \r\n\r\n");
            }
            catch { }

            string strResponse = "";
            string url = "";
            try
            {
                if(returnId==true)
                    url = My.serverAddress + "executor_return_id.php?a=" + clean(sql_to_send)+ "&b=" + clean(end_sql);
                else
                    url = My.serverAddress + "executor.php?a=" + clean(sql_to_send);
                                
                strResponse= myRequest(url);
            }
            catch (Exception ee)
            {
                MessageBox.Show("Internet Error: "+ee.Message+"\n" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }
            if (!strResponse.StartsWith("fliks") & !strResponse.StartsWith("\nfliks") & returnId == false)
            {
                try
                {
                    string bizlog = DateTime.Now.ToString("HH-mm-ss");
                    File.AppendAllText(My.logFileName, bizlog + ": ERROR\r\n" + strResponse + " \r\n\r\n");
                }
                catch { }
                MessageBox.Show("Data Error " + System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ((strResponse.Length > 40) ? strResponse.Substring(0, 40) : strResponse));
                    return null;
            }
            return strResponse;
        }

        public class jsonify
        {
            ArrayList a = new ArrayList();
            ArrayList b = new ArrayList();

            public jsonify Add(string a1, string b1)
            {
                a.Add(a1);
                b.Add(b1);
                return this;
            }

            public string finalize()
            {
                string res = "";
                for (int i = 0; i < a.Count; i++)
                {
                    res += ", \"" + a[i] + "\":\"" + b[i] + "\"";
                }
                if (res.Length > 0)
                    res = "{" + res.Substring(1) + "}";
                else
                    res = "{}";

                return res;
            }
        }


        public static void sqlSelectorAsync(String sql, Func<dynamic, int> populateDatagrid, String phpFile = "selector.php?a=",bool isFullURL=false)
        {
            Thread thread = new Thread(() =>
            {
                dynamic dynJson = sqlSelector(sql, phpFile,isFullURL);

                Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Normal,
                (Action)(() => populateDatagrid(dynJson)));
            });
            thread.Start();
        }

        public static void urlAsync(string url, Func<string, int> populateDatagrid)
        {
            Thread thread = new Thread(() =>
            {
                string dynJson = fetchUrl(url);

                Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Normal,
                (Action)(() => populateDatagrid(dynJson)));
            });
            thread.Start();
        }

        public class insertInto
        {
            string sqlText;
            ArrayList a = new ArrayList();
            ArrayList b = new ArrayList();
            public insertInto(string tableName)
            {
                sqlText = "INSERT INTO " + tableName + "(";
            }
            public insertInto Add(string a1, string b1)
            {
                a.Add(a1);
                b.Add(b1);
                return this;
            }

            public string finalize()
            {
                string sql1 = "";
                string sql2 = "";
                for (int i = 0; i < a.Count; i++)
                {
                    sql1 += "," + a[i];
                    sql2 += ",'" + b[i] + "'";
                }

                return sqlText + sql1.Substring(1) + ") VALUES (" + sql2.Substring(1) + ")";
            }


        }


        public class getRequest
        {
            ArrayList a = new ArrayList();
            ArrayList b = new ArrayList();
            string baseUrl;

            public getRequest Add(string a1, string b1)
            {
                a.Add(a1);
                b.Add(b1.Replace("&", "and"));
                return this;
            }

            public getRequest(string baseUrl_) {
                baseUrl=baseUrl_;
            }

            public string finalize()
            {
                string text = "";
                for (int i = 0; i < a.Count; i++)
                {
                    text += "&" + a[i] + "=" + b[i];
                }

                return baseUrl+"?" + text.Substring(1);
            }
        }

        public static string clean(String sql)
        {
            return sql.Replace("'", "`").Replace("<", ">>").Replace("+", ">>>").Replace("&", "and").Replace("\"", "") ;
        }

        public static void sqlExecutor_AddColumn(String sql_to_send)
        {
            string strResponse = "";
            try
            {
                string url = My.serverAddress + "addcolumn.php?a=" + sql_to_send;
                
                strResponse = myRequest(url);
            }
            catch (Exception ee)
            {
                MessageBox.Show("Internet Error " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return;
            }
            if (!strResponse.StartsWith("fliks") & !strResponse.StartsWith("Duplicate column"))
            {
                MessageBox.Show("Data Error " + System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public static string myRequest(string url)
        {
            return myRequest(url, My.unameApi, My.passApi);
        }

        public static string myRequest(string url,string uname, string pwd) 
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(uname + ":" + pwd));
            request.Headers.Add("Authorization", "Basic " + encoded);
            request.Headers["techfliks"]=My.fingerPrint;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();
            StreamReader readers = new StreamReader(resStream);
            string strResponse = readers.ReadToEnd();
            return strResponse.StartsWith("\n") == true ? strResponse.Substring(1) : strResponse;
        }

        public static dynamic sqlSelector(String sql, String phpFile = "selector.php?a=",bool isFullURL=false)
        {
            try
            {
                string bizlog = DateTime.Now.ToString("HH-mm-ss");
                File.AppendAllText(My.logFileName, bizlog + ": \r\n" + sql + " \r\n\r\n");
            }
            catch { }

            string strResponse = "";
            string url;
            try
            {
                url = ((isFullURL == false)?My.serverAddress:"") + phpFile + sql.Replace("'", "`").Replace("<", ">>").Replace("+", ">>>").Replace("\"", "");
                if (phpFile == "selector.php?a=")
                    url = url.Replace("&", "and");
                
                strResponse = myRequest(url); 
            }
            catch (Exception ee)
            {
                //MessageBox.Show("Internet Error: " + ee.Message + "\n" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }
            if (!strResponse.StartsWith("{") & !strResponse.StartsWith("[") & !strResponse.StartsWith("null"))
            {
                MessageBox.Show("Data Error " + System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ((strResponse.Length > 40) ? strResponse.Substring(0, 40) : strResponse));
            }

            try
            {
                dynamic dynJson = JsonConvert.DeserializeObject(strResponse);
                return dynJson;
            }
            catch (Exception eer)
            {
                MessageBox.Show("JSon Error " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string fetchUrl(string myurl)
        {
            try
            {
                string bizlog = DateTime.Now.ToString("HH-mm-ss");
                File.AppendAllText(My.logFileName, bizlog + ": \r\n" + myurl + " \r\n\r\n");
            }
            catch { }

            string strResponse = "";
            
            try
            {
                strResponse = myRequest(myurl,My.unameDocs,My.passDocs);
            }
            catch (Exception ee)
            {
                MessageBox.Show("Internet Error: " + ee.Message + "\n" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

            return strResponse;
        }


        public static void sqlSelectorAsyncPro(String sql, Func<dynamic, int> populateDatagrid, String phpFile = "selector_pro.php")
        {
            Thread thread = new Thread(() =>
            {
                dynamic dynJson = sqlSelectorPro(sql, phpFile);

                Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Normal,
                (Action)(() => populateDatagrid(dynJson)));
            });
            thread.Start();
        }

        public static bool timeChecker(String time, Boolean retry = true)
        {
            string strResponse = "";
            string url = "";
            try
            {
                url = My.serverAddress + "time_checker.php?a=" + time + "&b=" + My.settingsValue("Timezone");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();
                StreamReader readers = new StreamReader(resStream);
                strResponse = readers.ReadToEnd();
            }
            catch (Exception ee)
            {
                MessageBox.Show("Internet Error: " + ee.Message);
                return false;
            }
            if (!strResponse.StartsWith("fliks"))
            {

                if (retry)
                {
                    Thread.Sleep(100);
                    return timeChecker(time, false);
                }
                else
                {
                    MessageBox.Show("Please set Date and Time of the Computer");
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public static dynamic sqlSelectorPro(String sql, String phpFile = "selector_pro.php", Boolean retry = false)
        {
            string strResponse = "";
            string url;
            try
            {
                url = My.serverAddress + phpFile;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Credentials = new NetworkCredential(My.unameApi, My.passApi);

                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = new jsonify().Add("a", sql).finalize();

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();
                StreamReader readers = new StreamReader(resStream);
                strResponse = readers.ReadToEnd();
            }
            catch (Exception ee)
            {
                if (retry == true)
                {
                    MessageBox.Show("Internet Error");
                    return null;
                }
                else
                    return sqlSelectorPro(sql, phpFile, true);
            }
            if (!strResponse.StartsWith("{") & !strResponse.StartsWith("[") & !strResponse.StartsWith("null"))
            {
                MessageBox.Show("Data Error: " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

            try
            {
                dynamic dynJson = JsonConvert.DeserializeObject(strResponse);
                return dynJson;
            }
            catch (Exception eer)
            {
                MessageBox.Show("JSon Error");
                return null;
            }
        }
    }
}
