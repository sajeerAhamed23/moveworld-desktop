﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
//using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Warehouse
{
    public class My
    {
        private static My me;
        public static string shop = "MoveWorld";
        public static string user;
        public static string dburl = "D:\\POSfliks\\POSfliks\\bin\\Debug";
        public static string profit; 
        private static Dictionary<string, string> confJson = null;
        public static navs parent;
        public static string type;
        public static Dictionary<string, string> settingsJson = null;
        public static string[] settingsFieldLabel = new String[] {  "Name of the Company", "qatar OR kuwait" , "Defaulf Username"};
        public static string[] settingsField= new String[] { "name", "Branch" , "default_username" };
        public static string LogFileName;
        public static string logFileName;
        public static string errorFileName;
        public static string fingerPrint;
        public static string[] colsStock = new String[] { "invoice_id", "territory_code", "territory_name", "district_code", "district_name", "division", "div_name", "group_no", "group_name", "dept", "dept_name", "class", "class_name", "subclass", "sub_name", "brand_code", "brand_name", "style_number", "item_color", "colour_family", "item_shade", "item_size", "item_parent_code", "item_code", "barcode", "pack_size", "item_description", "current_season_number", "current_season_name", "current_phase_number", "current_phase_name", "storyline_collection_theme", "brand_ownership_royalty_terms", "brand_principal", "buyer_name", "fashion_tier", "price_tier", "generic_name", "material", "made_of", "percentage_composition", "quantity_ratio", "size_range", "display", "product_title", "occasion", "type", "pattern", "accents", "width", "theme", "closure", "care_instructions", "cost_price1", "cost_price2", "msrp_amount", "current_price", "sale_price", "auth", "noof", "price", "cost" };
        public static string[] colsStockAttr = new String[] { "invoice_id", "territory_code", "territory_name", "district_code", "district_name", "division", "div_name", "group_no", "group_name", "dept", "dept_name", "class", "class_name", "subclass", "sub_name", "brand_code", "brand_name", "style_number", "item_color", "colour_family", "item_shade", "item_size", "item_parent_code", "item_code", "barcode", "pack_size", "item_description", "current_season_number", "current_season_name", "current_phase_number", "current_phase_name", "storyline_collection_theme", "brand_ownership_royalty_terms", "brand_principal", "buyer_name", "fashion_tier", "price_tier", "generic_name", "material", "made_of", "percentage_composition", "quantity_ratio", "size_range", "display", "product_title", "occasion", "type", "pattern", "accents", "width", "theme", "closure", "care_instructions", "cost_price1", "cost_price2", "msrp_amount", "current_price", "sale_price" };
        public static string[] colsStockAttrMin = new String[] { "invoice_id", "item_code", "item_description" };

        public static string[] Months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

        public static string jpgLocation = "params/";

        public static string serverAddress = "https://laksiri.cf/api/";
        public static string rawServerAddress = "https://laksiri.cf/";
        public static string groupServerAddress = "https://laksiri.cf/";
        public static string FTPaddress = "ftp://laksiri.cf/";
        public static string FTPusername = "xyz@laksiri.cf";
        public static string FTPpassword = "1qazXSW@";
        public static string passApi = "VBNEW@#$5323fdss";
        public static string unameApi = "sad@#$232342sadads";
        public static string passDocs = "zxc@#$56dfg766j";
        public static string unameDocs = "asda@#$5654gfhrt";

        private My(string s)
        {
            shop = s;
        }

        public static string ss(string s)
        {
            
            s = s.Replace("'", " ");
            s = s.Replace("`", " ");
            s = s.Replace("<", " ");
            s = s.Replace(">", " ");
            s = s.Replace("+", " ");
            s = s.Replace("&", " ");
            return s;
        }

        public static void fitToNavs(UserControl wind)
        {
            double x = SystemParameters.VirtualScreenWidth;
            double y = SystemParameters.VirtualScreenHeight;

            if (y > x)
            {
                y = x;
                x = SystemParameters.VirtualScreenHeight;
            }

            wind.Height = 15 * y / 16;
            wind.Width = 7 * x / 8;
        }

        public static Visibility configVisibility(string param)
        {
            return (configValue(param) == "1") ? Visibility.Visible : Visibility.Hidden;
        }

        public static string configValue(string param)
        {
            if (confJson == null)
            {
                try
                {
                    string fromFile = File.ReadAllText("Params/config.json");
                    fromFile = fromFile.Replace("\r\n", "").Replace("\t", "").Replace(",}", "}");
                    dynamic json = JsonConvert.DeserializeObject(fromFile);
                    confJson = JsonConvert.DeserializeObject<Dictionary<string, string>>(My.s(json));
                }
                catch (Exception d)
                {
                }
            }
            string x = "";
            try
            {
                x = confJson[param];
            }
            catch { }
            return x;
        }

        public static string settingsValue(string param)
        {
            if (settingsJson == null)
            {
                try
                {
                    string fromFile = File.ReadAllText("Params/settings.json");
                    fromFile = fromFile.Replace("\r\n", "").Replace("\t", "").Replace(",}", "}");
                    dynamic json = JsonConvert.DeserializeObject(fromFile);
                    settingsJson = JsonConvert.DeserializeObject<Dictionary<string, string>>(My.s(json));
                }
                catch (Exception d)
                {
                }
            }
            string x = "";
            try
            {
                x = settingsJson[param];
            }
            catch { }
            return x;
        }

        public static ArrayList initCombo(string tableName, string colname)
        {
            string sql = "SELECT distinct " + colname + " as x FROM " + tableName + " WHERE NOT " + colname + " =''";
            dynamic dynJson = Mydb.sqlSelector(sql);

            ArrayList productList = new ArrayList();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    productList.Add((My.s(i.x)));
                }
            }
            return productList;
        }

        public static ArrayList initComboWithIDs(string tableName, string colname, ref ArrayList ids)
        {
            string sql = "SELECT distinct " + colname + " as x, id FROM " + tableName + " WHERE NOT " + colname + " =''";
            dynamic dynJson = Mydb.sqlSelector(sql);

            ArrayList productList = new ArrayList();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    productList.Add((My.s(i.x)));
                    ids.Add((My.s(i.id)));
                }
            }
            return productList;
        }

        public static float totalizer(DataTable dtin, string col)
        {
            float sum = 0;
            for (int i = 0; i < dtin.Rows.Count; i++)
            {
                string val = dtin.Rows[i][col].ToString();
                float x = My.f(val);
                sum += x;
            }
            return sum;
        }

        public static float f(object s, float defaultVal = 0)
        {
            return s==null?0:f(s.ToString(), defaultVal);
        }

        public static string _n(string s)
        {
            s = s.Replace(" ", "_");
            s = s.ToLower();
            return s;
        }

        public static string _N(string s)
        {
            s = s.Replace("_", " ");
            s = Regex.Replace(s, @"(^\w)|(\s\w)", m => m.Value.ToUpper());
            return s;
        }

        public static float f(string s, float defaultVal)
        {
            float x;
            try
            {
                x = float.Parse(s);
            }
            catch (Exception eer)
            {
#if POSfliks

#else
                if (s != "")
                    MessageBox.Show("Wrong Number: You output may be wrong", "");
#endif
                x = defaultVal;
            }
            return x;
        }

      

        public static string appendSQL(TextBox c, string colname)
        {
            if (c.Text != "")
            {
                return " AND " + colname + " = '" + c.Text + "'";
            }
            else
                return "";
        }

        public static string appendSQL(ComboBox c, string colname)
        {
            if (c.Text != "")
            {
                return " AND " + colname + " = '" + c.Text + "'";
            }
            else
                return "";
        }

        public static void setLogFileName(string s)
        {
            if (me == null)
            {
                me = new My("POSfliks");
            }
            LogFileName = s;
        }

        public static string currency()
        {
            string name = My.settingsValue("Branch");
            if (name == "kuwait")
            {
                return "KD";
            }
            else if (name == "qatar")
            {
                return "QAR";
            }
            return "";
        }


        public static int colID(DataTable dt, string col)
        {
            try
            {
                string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
                int colid = Array.IndexOf(columnNames, col);

#if POSfliks

#else

                if (colid == -1)
                {
                    MessageBox.Show("Minor Error: Wrong Column \r\nPls contact developers", "");
                }

#endif


                return colid;
            }
            catch (Exception ee) { return -1; }
        }

        public static Boolean authInsert()
        {
            if (type == "Admin" | type == "User" | type == "Boss")
            {
                return true;
            }
            MessageBox.Show("You are not an Authorized user!", My.shop);
            return false;
        }

        public static Boolean authAppend()
        {
            if (type == "Admin" | type == "Boss")
            {
                return true;
            }
            MessageBox.Show("You are not an Authorized!", My.shop);
            return false;
        }

        public static Boolean authBoss()
        {
            if (type == "Boss")
            {
                return true;
            }
            MessageBox.Show("You are not Authorized", My.shop);
            return false;
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static String s(Object any)
        {
            if (any == null)
                return "";
            return (String)any.ToString();
        }
        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words.ToUpper();
        }


        public static void sendeFile(string file, string path, string createFolderName="")
        {
            /*
             * Sample usage: 
             * My.sendeFile("s.txt","params/config.json","alldocsoflaksiri/hbl/1031/");
             */

            using (WebClient client = new WebClient())
            {
                if (createFolderName != "")
                {
                    WebRequest ftpRequest = WebRequest.Create(My.FTPaddress + createFolderName);
                    ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                    ftpRequest.Credentials = new NetworkCredential(FTPusername, FTPpassword);
                    try
                    {
                        using (var resp = (FtpWebResponse)ftpRequest.GetResponse())
                        {
                            Console.WriteLine(resp.StatusCode);
                        }
                    }
                    catch { }
                }

                client.Credentials = new NetworkCredential(FTPusername, FTPpassword);
                client.UploadFile(My.FTPaddress + createFolderName + file, "STOR", path);

                MessageBox.Show("File uploaded. Please check ", My.shop);
            }
        }        

        public static void createRow(DataTable dtThis, string heading, string property)
        {
            DataRow row = dtThis.NewRow();
            row[heading] = property;
            dtThis.Rows.Add(row);
        }

        public static void createBooleanRow(DataTable dtThis, string heading, Boolean property)
        {
            DataRow row = dtThis.NewRow();
            row[heading] = property;
            dtThis.Rows.Add(row);
        }
                
        public static void log(string name, string additional)
        {
            string bizlog = DateTime.Now.ToString("HH-mm-ss");
            try
            {
                File.AppendAllText(My.logFileName, bizlog + ": " + name + " - " + additional + " \r\n\r\n");
            }
            catch { }
        }

        public static void logScreen(string prefix = "")
        {

            try
            {
                double screenLeft = SystemParameters.VirtualScreenLeft;
                double screenTop = SystemParameters.VirtualScreenTop;
                double screenWidth = SystemParameters.VirtualScreenWidth;
                double screenHeight = SystemParameters.VirtualScreenHeight;
                using (Bitmap bmp = new Bitmap((int)screenWidth,
                    (int)screenHeight))
                {
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        String filename = "Log-" + prefix + " " + DateTime.Now.ToString("yyyyMMdd-hhmmss") + ".png";
                        //Opacity = .0;
                        g.CopyFromScreen((int)screenLeft, (int)screenTop, 0, 0, bmp.Size);
                        bmp.Save("Logs\\Screenshots\\" + filename);
                        //Opacity = 1;
                    }
                }
            }
            catch { }
        }


        public static void handleException(Exception bizee, string func, object thisobj)
        {
            string bizlog = DateTime.Now.ToString("HH-mm-ss") + ": " + bizee.Message;
            MessageBox.Show("ERROR: " + thisobj.GetType().Name + "." + func + " " + bizlog, My.shop);
            try
            {
                File.AppendAllText(My.errorFileName, bizlog + ": " + thisobj.GetType().Name + "." + func + " \r\n\r\n");
            }
            catch { }
        }

    }
}
