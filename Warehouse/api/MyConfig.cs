﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Warehouse
{
    class MyConfig
    {
        private static Dictionary<string, string> serverJson = null;

        public static string serverValue(string param)
        {
            if (serverJson == null)
            {
                try
                {
                    string fromFile = Mydb.fetchUrl(My.groupServerAddress + "configs/?who=" + My.settingsValue("Branch"));
                    fromFile = fromFile.Replace("\r\n", "").Replace("\t", "").Replace(",}", "}");
                    dynamic json = JsonConvert.DeserializeObject(fromFile);
                    serverJson = JsonConvert.DeserializeObject<Dictionary<string, string>>(My.s(json));
                }
                catch (Exception d)
                {
                }
            }
            string x = "";
            try
            {
                x = serverJson[param];
            }
            catch
            {
                MessageBox.Show("Unknown server-configuration. Please infrom to Techfliks: " + param);
            }
            return x;
        }

    }
}
