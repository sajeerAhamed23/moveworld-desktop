﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for MyFilter.xaml
    /// </summary>
    public partial class MyLoader : UserControl
    {
        #region Label DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String Label
        {
            get { return (String)GetValue(HeadingProperty); }
            set { SetValue(HeadingProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty HeadingProperty =
            DependencyProperty.Register("Label", typeof(string),
              typeof(MyLoader), new PropertyMetadata(""));

        #endregion

        #region Color DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String Color
        {
            get { return (String)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(string),
              typeof(MyLoader), new PropertyMetadata(""));

        #endregion


        #region OtherColor DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String OtherColor
        {
            get { return (String)GetValue(OtherColorProperty); }
            set { SetValue(OtherColorProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty OtherColorProperty =
            DependencyProperty.Register("OtherColor", typeof(string),
              typeof(MyLoader), new PropertyMetadata(""));

        #endregion

        Thread thread;

        public MyLoader()
        {
            InitializeComponent();
        }

        public void Show()
        {
            Visibility = Visibility.Visible;
            string x = "";
            thread = new Thread(() =>
            {
                while (true)
                {
                    if (x.Length == 4)
                        x = "";
                    x += "●";
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke(
                        DispatcherPriority.Normal,
                        (Action)(() => { spinner.Content = x; }));
                        Thread.Sleep(1000);
                    }
                    catch { thread.Abort(); }
                }

            });
            thread.Start();
        }

        public void Hide()
        {
            try
            {
                thread.Abort();
            }
            catch
            {
                try
                {
                    thread.Abort();
                }
                catch { }
            }
            Visibility = Visibility.Hidden;
        }

        private void Label_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Hide();
        }

        private void spinner_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Hide();
        }

        private void Grid_MouseEnter_1(object sender, MouseButtonEventArgs e)
        {
            Hide();
        }



    }
}
