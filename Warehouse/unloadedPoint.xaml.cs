﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for unloadedPoint.xaml
    /// </summary>
    public partial class unloadedPoint : UserControl
    {

        DataTable dtMain,dtDown,dtSelected;
        ArrayList alreadyUnloaded = new ArrayList();

        public unloadedPoint()
        {
            InitializeComponent();

            dtMain = new DataTable();
            dtMain.Columns.Add("ID").ReadOnly = true;
            dtMain.Columns.Add("Name").ReadOnly = true;
            dtMain.Columns.Add("Ref").ReadOnly = true;
            dtMain.Columns.Add("Country").ReadOnly = true;
            dtMain.Columns.Add("Cargo Type").ReadOnly = true;
            dtMain.Columns.Add("BL Number");
            dtMain.Columns.Add("Created Time").ReadOnly = true;
            dtMain.Columns.Add("Created Date").ReadOnly = true;
            //dtMain.Columns.Add("Has Reached").ReadOnly = true;
            dtMain.Columns.Add("ETA");


            dtSelected = new DataTable();
            dtSelected.Columns.Add("X", typeof(Boolean));
            dtSelected.Columns.Add("HBL").ReadOnly = true;
            dtSelected.Columns.Add("Type").ReadOnly = true;
            dtSelected.Columns.Add("Volume").ReadOnly = true;
            dtSelected.Columns.Add("Quantity").ReadOnly = true;
            dtSelected.Columns.Add("Weight").ReadOnly = true;
            dtSelected.Columns.Add("Name").ReadOnly = true;
            dtSelected.Columns.Add("Zone").DefaultValue = "Zone A";
            dtSelected.Columns.Add("PID").ReadOnly = true;
            
            dtDown = new DataTable();
            dtDown.Columns.Add("X", typeof(Boolean));
            dtDown.Columns.Add("HBL").ReadOnly = true;
            dtDown.Columns.Add("Type").ReadOnly = true;
            dtDown.Columns.Add("Volume").ReadOnly = true;
            dtDown.Columns.Add("Quantity").ReadOnly = true;
            dtDown.Columns.Add("Weight").ReadOnly = true;
            dtDown.Columns.Add("Name").ReadOnly = true;
            dtDown.Columns.Add("PID").ReadOnly = true;
            
            CBname.ItemsSource = My.initCombo("loading", "ref");
            datestart1.SelectedDate = DateTime.Now.AddMonths(-2);
            dateend.SelectedDate = DateTime.Now.AddDays(1);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            My.fitToNavs(this);
        }

        private void Bsubmit_Click(object sender, RoutedEventArgs e)
        {
            initer();
            My.log(((Button)sender).Content.ToString(), "Unload Point");
        }

        public void initer()
        {
            String extra = (CBname.Text != "") ? " AND ref = ' " + CBname.Text + "'" : " ";
            extra += (isAir.IsChecked == true) ? " AND isAir = '1' " : " AND isAir = '0' ";
            if (TblNum.Text != "") extra += " AND bl_number='" + TblNum.Text + "'";
            string tds = DateTime.Parse(datestart1.Text).ToString("yyyy-MM-dd");
            string tde = DateTime.Parse(dateend.Text).ToString("yyyy-MM-dd");

            string sql = "SELECT *,ref as refe FROM loading WHERE is_deleted=0 AND fully_unloaded=0 AND date_>='" + tds + "' AND date_<='" + tde + "' " + extra;

            loader.Show();
            Mydb.sqlSelectorAsync(sql, populate);
        }

        private int populate(dynamic dynJson)
        {
#if POSfliks
            try{
#endif
            dtMain.Rows.Clear();
            dtSelected.Rows.Clear();
            dtDown.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    DataRow row = dtMain.NewRow();
                    row["ID"] = i.id;
                    row["Ref"] = i.refe;
                    row["Country"] = i.country;
                    row["Name"] = i.notes;
                    row["Cargo Type"] = My.s(i.isAir) == "1" ? "Air Cargo" : "Sea Cargo";
                    //row["Has Reached"] = My.s(i.reached) == "1" ? "Yes" : "No";
                    row["Created Time"] = i.time_;
                    row["Created Date"] = i.date_;
                    row["ETA"] = i.reached_date;
                    row["BL Number"] = i.bl_number;

                    dtMain.Rows.Add(row);
                }
            }

            tableMain.ItemsSource = dtMain.DefaultView;

            try
            {

                tableMain.Columns[My.colID(dtMain, "ID")].Width = new DataGridLength(30);
            }
            catch (Exception eer) { }

            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }

        String country = "";
        private void tableMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView dataRow = (DataRowView)tableMain.SelectedItem;
                string id = dataRow.Row.ItemArray[My.colID(dtMain, "Ref")].ToString();
                country = dataRow.Row.ItemArray[My.colID(dtMain, "Country")].ToString();

                initerDown(id);
            }
            catch (Exception es) { }
        }

        private void tableDown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRow dataRow = ((DataRowView)tableDown.SelectedItem).Row;

                MessageBoxResult re = MessageBox.Show("Do you want to Load?", My.shop, MessageBoxButton.OKCancel);
                if (re == MessageBoxResult.OK)
                {
                    dtSelected.ImportRow(dataRow);
                    tableSelected.ItemsSource = dtSelected.DefaultView;
                    dtDown.Rows.Remove(dataRow);
                }
            }
            catch (Exception ee) { }
        }

        /*private void tableSelected_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRow dataRow = ((DataRowView)tableSelected.SelectedItem).Row;

                MessageBoxResult re = MessageBox.Show("Do you want to unload?", My.shop, MessageBoxButton.OKCancel);
                if (re == MessageBoxResult.OK)
                {
                    dtDown.ImportRow(dataRow);

                    tableDown.ItemsSource = dtDown.DefaultView;
                    dtSelected.Rows.Remove(dataRow);
                }
            }
            catch (Exception ee) { }
        }*/

        private void BaddAll_Click(object sender, RoutedEventArgs e)
        {
            tableDown.SelectedIndex = -1;
            tableSelected.SelectedIndex = -1;

            for (int i = 0; i < tableDown.Items.Count; i++)
            {
                DataRowView dataRow = (DataRowView)tableDown.Items[i];

                dtSelected.ImportRow(dataRow.Row);
                tableSelected.ItemsSource = dtSelected.DefaultView;

            }

            dtDown.Rows.Clear();
            tableDown.ItemsSource = dtDown.DefaultView;

            My.log(((Button)sender).Content.ToString(), "Assign");
        }

        private void Breset_Click(object sender, RoutedEventArgs e)
        {
            reset();
            My.log(((Button)sender).Content.ToString(), "Assign");
        }

        private void reset()
        {
            dtDown.Rows.Clear();
            dtSelected.Rows.Clear();

            tableDown.ItemsSource = dtDown.DefaultView;
            tableSelected.ItemsSource = dtSelected.DefaultView;
        }

        private void Bassign_Click(object sender, RoutedEventArgs e)
        {
            ArrayList ids = new ArrayList() { 0,1,2,3 };        //new ArrayList();
            ArrayList zones = new ArrayList() {"Zone A","Zone B","Zone C","Zone D" };          //My.initComboWithIDs("package", "zone", ref ids);

            int zone_id = 1;
            InputCombo inputDialog = new InputCombo("Select the Zone?", zones, ids);
            if (inputDialog.ShowDialog() == true)
            {
                zone_id = Int32.Parse(inputDialog.Answer.ToString());
                dtSelected.Rows[tableSelected.SelectedIndex].SetField("Zone", zones[zone_id]);
            }
            else
            {
                tableSelected.Focus();
                return;
            }

            int x = tableSelected.Items.Count;
            if (x == 0)
            {
                MessageBox.Show("Please select atleast one package", My.shop);
                return;
            }

            /*string update = "UPDATE package SET zone='" + zone_id + "' WHERE id in (";
            for (int i = 0; i < x; i++)
            {
                DataRowView dataRow1 = (DataRowView)tableSelected.Items[i];
                if (dataRow1.Row.ItemArray[My.colID(dtSelected, "X")].ToString() == "True")
                {

                    DataRowView dataRow = (DataRowView)tableSelected.Items[i];
                    string id = dataRow.Row.ItemArray[My.colID(dtSelected, "HBL")].ToString();

                    update += id + ",";
                }
            }
            update += "-1)";
            Mydb.sqlExecutor(update);*/


            tableSelected.ItemsSource = dtSelected.DefaultView;

            My.log(((Button)sender).Content.ToString(), "assign zone");
        }

        private void BaddAll_Copy_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRowOuter = (DataRowView)tableMain.SelectedItem;
            string loading_id = dataRowOuter.Row.ItemArray[My.colID(dtMain, "ID")].ToString();
            
            String sql = "";
            for (int i = 0; i < tableSelected.Items.Count; i++)
            {
                DataRowView dataRow = (DataRowView)tableSelected.Items[i];
                string hid = dataRow.Row.ItemArray[My.colID(dtSelected, "HBL")].ToString();
                string pid = dataRow.Row.ItemArray[My.colID(dtSelected, "PID")].ToString();
                string zone = dataRow.Row.ItemArray[My.colID(dtSelected, "Zone")].ToString();

                string sqlTemp = new Mydb.insertInto("package")
                    .Add("country", country)
                    .Add("loading_id", loading_id)
                    .Add("hbl_id", hid)
                    .Add("package_id", pid)
                    .Add("zone", zone)
                    .Add("notes", "")
                    .finalize();

                sql += sqlTemp + ";";
            }

            Mydb.sqlExecutor(sql);


            //update if all packages are transfered
            if (tableDown.Items.Count == 0)
            {
                String sql1 = "update loading set fully_unloaded=1 where id=" + loading_id;
                Mydb.sqlExecutor(sql1);
            }

            reset();
            dtMain.Rows.Clear();
            tableMain.ItemsSource = dtMain.DefaultView;
        }

        private void initerDown(string loading_id)
        {
            alreadyUnloaded = new ArrayList();
            //best one: tables broken: dynamic dynJson = Mydb.sqlSelector("select * from package, hbl where package.hbl_id = hbl.id and hbl.loading_id = " + loading_id + " AND hbl.country='" + country + "'");
            // this one all in pakcage table
            dynamic dynJson = Mydb.sqlSelector("select * from package where loading_id = " + loading_id + " AND country='" + country + "'");

            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    alreadyUnloaded.Add(i.hbl_id + "-" + i.package_id);
                }
            }

            string sql = "SELECT cargo.id as idh,package.id as idp,pickedup_date, package.type as typePack, name, (length * breadth * heigth * quantity) as volume, (quantity) as quantity_tot, (weigth) as weight_tot, pickup_date FROM cargo,package where cargo.id = package.cargo_id and loading_id =" + loading_id;

            loader.Show();
            Mydbs.sqlSelectorAsync(country, sql, populateDown);
        }

        private int populateDown(dynamic dynJson)
        {
#if POSfliks
            try{
#endif
            dtDown.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    DataRow row = dtDown.NewRow();

                    if (alreadyUnloaded.Contains(i.idh + "-" + i.idp) == true)
                        continue;
                    row["HBL"] = i.idh;
                    row["PID"] = i.idp;
                    row["Type"] = i.typePack;
                    row["Volume"] = i.volume;
                    row["Quantity"] = i.quantity;
                    row["Weight"] = i.weight_tot;
                    row["Name"] = i.name;
                    row["X"] = false;

                    dtDown.Rows.Add(row);
                }
            }

            tableDown.ItemsSource = dtDown.DefaultView;

            try
            {
                tableDown.Columns[My.colID(dtDown, "X")].Width = new DataGridLength(30);
            }
            catch (Exception eer) { }
            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }


    }
}
