﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.IO;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for reachedShipment.xaml
    /// </summary>
    public partial class reachedShipment : UserControl
    {

        DataTable dtMain, dtDown, dtDownSummary;

        public reachedShipment()
        {
            InitializeComponent();


            dtMain = new DataTable();
            dtMain.Columns.Add("ID").ReadOnly = true;
            dtMain.Columns.Add("Name").ReadOnly = true;
            dtMain.Columns.Add("Ref").ReadOnly = true;
            dtMain.Columns.Add("Country").ReadOnly = true;
            dtMain.Columns.Add("Cargo Type").ReadOnly = true;
            dtMain.Columns.Add("BL Number");
            dtMain.Columns.Add("Created Time").ReadOnly = true;
            dtMain.Columns.Add("Created Date").ReadOnly = true;
            //dtMain.Columns.Add("Has Reached").ReadOnly = true;
            dtMain.Columns.Add("ETA");



            dtDown = new DataTable();
            dtDown.Columns.Add("HBL").ReadOnly = true;
            dtDown.Columns.Add("Type").ReadOnly = true;
            dtDown.Columns.Add("Volume").ReadOnly = true;
            dtDown.Columns.Add("Quantity").ReadOnly = true;
            dtDown.Columns.Add("Weight").ReadOnly = true;
            dtDown.Columns.Add("Name").ReadOnly = true;
            dtDown.Columns.Add("PID").ReadOnly = true;


            dtDownSummary = new DataTable();
            dtDownSummary.Columns.Add("Type").ReadOnly = true;
            dtDownSummary.Columns.Add("Value").ReadOnly = true;

            datestart1.SelectedDate = DateTime.Now.AddMonths(-2);
            dateend.SelectedDate = DateTime.Now.AddDays(1);
        }

        private void content_Loaded(object sender, RoutedEventArgs e)
        {
            My.fitToNavs(this);
        }

        private void Bsubmit_Click(object sender, RoutedEventArgs e)
        {
            initer();
        }

        public void initer()
        {
            string tds = DateTime.Parse(datestart1.Text).ToString("yyyy-MM-dd");
            string tde = DateTime.Parse(dateend.Text).ToString("yyyy-MM-dd");

            string sql = "SELECT *,ref as refe FROM loading WHERE is_deleted=0 AND date_>='" + tds + "' AND date_<='" + tde + "'";

            loader.Show();
            Mydb.sqlSelectorAsync(sql, populate);
        }

        String country = "";
        private void table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView dataRow = (DataRowView)table.SelectedItem;
                string id = dataRow.Row.ItemArray[My.colID(dtMain, "Ref")].ToString();
                country = dataRow.Row.ItemArray[My.colID(dtMain, "Country")].ToString();

                initerDown(id);
            }
            catch (Exception es) { }
        }

        private int populate(dynamic dynJson)
        {
#if POSfliks
            try{
#endif
            dtMain.Rows.Clear();
            dtDown.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    DataRow row = dtMain.NewRow();
                    row["ID"] = i.id;
                    row["Ref"] = i.refe;
                    row["Country"] = i.country;
                    row["Name"] = i.notes;
                    row["Cargo Type"] = My.s(i.isAir) == "1" ? "Air Cargo" : "Sea Cargo";
                    //row["Has Reached"] = My.s(i.reached) == "1" ? "Yes" : "No";
                    row["Created Time"] = i.time_;
                    row["Created Date"] = i.date_;
                    row["ETA"] = i.reached_date;
                    row["BL Number"] = i.bl_number;

                    dtMain.Rows.Add(row);
                }
            }

            table.ItemsSource = dtMain.DefaultView;

            try
            {

                table.Columns[My.colID(dtMain, "ID")].Width = new DataGridLength(30);
            }
            catch (Exception eer) { }

            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }


        private void initerDown(string loading_id)
        {
            
            string sql = "SELECT cargo.id as idh,package.id as idp,pickedup_date, package.type as typePack, name, (length * breadth * heigth * quantity) as volume, (quantity) as quantity_tot, (weigth) as weight_tot, pickup_date FROM cargo,package where cargo.id = package.cargo_id and loading_id =" + loading_id;

            loader.Show();
            Mydbs.sqlSelectorAsync(country, sql, populateDown);
        }

        private int populateDown(dynamic dynJson)
        {
#if POSfliks
            try{
#endif
            dtDown.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    DataRow row = dtDown.NewRow();
                    
                    row["HBL"] = i.idh;
                    row["PID"] = i.idp;
                    row["Type"] = i.typePack;
                    row["Volume"] = i.volume;
                    row["Quantity"] = i.quantity;
                    row["Weight"] = i.weight_tot;
                    row["Name"] = i.name;

                    dtDown.Rows.Add(row);
                }
            }

            tableDown.ItemsSource = dtDown.DefaultView;
            populateDownSummary();
            try
            {
                tableDown.Columns[My.colID(dtDown, "HBL")].Width = new DataGridLength(30);
            }
            catch (Exception eer) { }
            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }

        private void populateDownSummary()
        {
            dtDownSummary.Rows.Clear();

            DataRowView dataRow = (DataRowView)table.SelectedItem;
            
            DataRow row = dtDownSummary.NewRow();
            row["Type"] = "Country";
            row["Value"] = dataRow.Row.ItemArray[My.colID(dtMain, "Country")].ToString();
            dtDownSummary.Rows.Add(row);

            DataRow row1 = dtDownSummary.NewRow();
            row1["Type"] = "Shipper Type";
            row1["Value"] = dataRow.Row.ItemArray[My.colID(dtMain, "Cargo Type")].ToString();
            dtDownSummary.Rows.Add(row1);

            DataRow row2 = dtDownSummary.NewRow();
            row2["Type"] = "ETA";
            row2["Value"] = dataRow.Row.ItemArray[My.colID(dtMain, "ETA")].ToString();
            dtDownSummary.Rows.Add(row2);

            DataRow row3 = dtDownSummary.NewRow();
            row3["Type"] = "No of Consignee";
            row3["Value"] = dtDown.Rows.Count;
            dtDownSummary.Rows.Add(row3);

            DataRow row3a = dtDownSummary.NewRow();
            row3a["Type"] = "Total Volume";
            row3a["Value"] = My.totalizer(dtDown, "Volume");
            dtDownSummary.Rows.Add(row3a);

            DataRow row4 = dtDownSummary.NewRow();
            row4["Type"] = "Total Wieght";
            row4["Value"] = My.totalizer(dtDown, "Weight");
            dtDownSummary.Rows.Add(row4);

            DataRow row5 = dtDownSummary.NewRow();
            row5["Type"] = "Total peices";
            row5["Value"] = My.totalizer(dtDown, "Quantity");
            dtDownSummary.Rows.Add(row5);

            tableDownSummary.ItemsSource = dtDownSummary.DefaultView;
        }

        private void BviewDocs_Click(object sender, RoutedEventArgs e)
        {
            string id = "";
            try
            {
                DataRowView dataRow = (DataRowView)table.SelectedItem;
                id = dataRow.Row.ItemArray[My.colID(dtMain, "Ref")].ToString();
            }
            catch (Exception es)
            {
                MessageBox.Show("Please Select a Shipment");
                return;
            }

            My.parent.viewDocsShipment1.Visibility = Visibility.Visible;
            My.parent.viewDocsShipment1.initer(id, "", "shipment",country);
        }
    }
}
