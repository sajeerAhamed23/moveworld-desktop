﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class navs : Window
    {
        public navs(String who)
        {
            InitializeComponent();
            Lusername.Content = My.settingsValue("Group") + " " + My.settingsValue("Branch")+" - Welcome! " + who;

        }


        private void Bexit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void Bmini_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void Bview_Click(object sender, RoutedEventArgs e)
        {
            if (My.authBoss() == false)
                return;

            hideAll();
            pending1.Visibility = Visibility.Visible;
            //pending1.initer();
            cutify(Bview, Brushes.Firebrick);

            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void hideAll()
        {
            for (int i = 0; i < allUserControls.Children.Count; i++)
            {
                allUserControls.Children[i].Visibility = Visibility.Hidden;
            }

            for (int i = 1; i < allButtons.Children.Count; i++)
            {
                Button b = (Button)allButtons.Children[i];
               
                Thickness m = b.Margin;
                m.Right = 10;
                b.Margin = m;
            }
            

        }

        private void BviewFetch_Click(object sender, RoutedEventArgs e)
        {
            if (My.authAppend() == false)
                return;

            hideAll();
            warehouse1.Visibility = Visibility.Visible;
            //warehouse1.initer();
            cutify(BviewFetch, Brushes.Firebrick);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void Window_ContentRendered_1(object sender, EventArgs e)
        {
            My.parent = this;
        }

        private void Beditmode_Click(object sender, RoutedEventArgs e)
        {            
            hideAll();

            editview1.Visibility = Visibility.Visible;
            //editview1.initer();
            cutify(Beditmode, Brushes.IndianRed);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void Bhelp_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Badmin_Click(object sender, RoutedEventArgs e)
        {
            if (My.authBoss() == false)
                return;

            hideAll();
            admin1.Visibility = Visibility.Visible;

        }
        
        private void Bassign_Click(object sender, RoutedEventArgs e)
        {
            if (My.authAppend() == false)
                return;
            hideAll();
            assign1.Visibility = Visibility.Visible;
            cutify(Bassign, Brushes.Chocolate);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void Bdrivers_Click(object sender, RoutedEventArgs e)
        {
            if (My.authBoss() == false)
                return;

            hideAll();
            driver1.Visibility = Visibility.Visible;
            driver1.initer();
            cutify(Bdrivers, Brushes.Chocolate);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void Bunverified_Click(object sender, RoutedEventArgs e)
        {
            if (My.authAppend() == false)
                return;

            hideAll();
            unverified1.Visibility = Visibility.Visible;
            //unverified1.initer();
            cutify(Bunverified, Brushes.Coral);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        

        private void cutify(Button but, Brush bru)
        {
            Thickness m = but.Margin;
            m.Right = 0;
            but.Margin = m;

            cute.Fill = bru;
        }

        private void Bmanifest_Click(object sender, RoutedEventArgs e)
        {

            hideAll();
            manifest1.Visibility = Visibility.Visible;
            //manifest1.initer();
            cutify(Bmanifest, Brushes.Chocolate);
            My.log(((Button)sender).Content.ToString(), "Navs");     
        }

        private void Bstatus_Click(object sender, RoutedEventArgs e)
        {
            hideAll();
            status1.Visibility = Visibility.Visible;
            cutify(Bstatus, Brushes.IndianRed);
            My.log(((Button)sender).Content.ToString(), "Navs"); 
        }

        

        private void BaddJob_Click(object sender, RoutedEventArgs e)
        {
            if (My.authAppend() == false)
                return;

            hideAll();
            addJob1.Visibility = Visibility.Visible;
            cutify(BaddJob, Brushes.Coral);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        private void Bcallrec_Click(object sender, RoutedEventArgs e)
        {
            if (My.authBoss() == false)
                return;

            hideAll();
            callRec1.Visibility = Visibility.Visible;
            //callRec1.initer();
            cutify(Bcallrec, Brushes.IndianRed);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

        

        private void BcancelledHBL_Click(object sender, RoutedEventArgs e)
        {
            if (My.authAppend() == false)
                return;

            hideAll();
            cancelledHbl1.Visibility = Visibility.Visible;
            //cancelledHbl1.initer();
            cutify(BcancelledHBL, Brushes.IndianRed);
            My.log(((Button)sender).Content.ToString(), "Navs");
        }

    }
}
