﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for loadedShipment.xaml
    /// </summary>
    public partial class loadedShipment : UserControl
    {
         

        public loadedShipment()
        {
            InitializeComponent();
             
        }

        private void datestart_Loaded(object sender, RoutedEventArgs e)
        {
            My.fitToNavs(this);
        }
    }
}