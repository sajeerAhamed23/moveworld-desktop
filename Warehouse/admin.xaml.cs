﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for admin.xaml
    /// </summary>
    public partial class admin : UserControl
    {
        navs parent;
        DataTable dt;
        public admin()
        {
            InitializeComponent();

            Jreports.fieldLabels = My.settingsFieldLabel;
            Jreports.json = My.settingsJson;
            Jreports.fields = My.settingsField;

            dt = new DataTable();
            dt.Columns.Add("ID").ReadOnly = true;
            dt.Columns.Add("Username");
            dt.Columns.Add("Type");
           
            ArrayList types = new ArrayList();
            types.Add("Boss");
            types.Add("Admin");
            types.Add("User");
            types.Add("Guest");
            Ctype.ItemsSource = types;

            initer();
        }

        public void initer()
        {
            dt.Rows.Clear();

            string sql = "SELECT * FROM users";
           // SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //QLiteDataReader reader = command.ExecuteReader();
             dynamic dynJson = Mydb.sqlSelector(sql);
            

            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {

                    DataRow row = dt.NewRow();

                    row["ID"] = i.id;
                    row["Username"] = i.username;
                    row["Type"] = i.type;
                   

                    dt.Rows.Add(row);
                }
            }
            table.ItemsSource = dt.DefaultView;

        }


        private void BnewPass_Click(object sender, RoutedEventArgs e)
        {
            Boolean passok = false;
            Boolean userok = false;
            string password = My.MD5Hash(p1.Password);
            string sql = "select password,type from users WHERE username='" + username.Text + "'";
            //SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //SQLiteDataReader reader = command.ExecuteReader();
            dynamic dynJson = Mydb.sqlSelector(sql);


            dt.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    userok = true;
                    DataRow row = dt.NewRow();
                    row["password"] = i.password;
                    String p = row["password"].ToString();
                    if (password == p)
                    {
                        passok = true;
                        if (p2.Password == p3.Password & p2.Password.Length > 3)
                        {
                            String sql1 = "update users set password ='" + My.MD5Hash(p2.Password) + "' WHERE username='" + username.Text + "';";
                            //SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
                            //command1.ExecuteNonQuery();
                            Mydb.sqlExecutor(sql1);
                            MessageBox.Show("New password set", My.shop);
                        }
                        else
                        {
                            MessageBox.Show("Passwords Don't Match or need more than 4 characters", My.shop);
                        }
                    }
                }

                if (userok)
                {
                    if (!passok)
                    {
                        MessageBox.Show("Current Password Wrong");
                    }
                }
                else
                {
                    MessageBox.Show("Username Error");
                }


            }
            My.log(((Button)sender).Content.ToString(), "Admin");
        }

        private void Bupdate_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult re = MessageBox.Show("Your current program will close! Click OK to Update", My.shop, MessageBoxButton.OKCancel);
            if (re == MessageBoxResult.OK)
            {
                Process.Start("FliksPad.exe");
                //parent.syncCheck.Abort();
                Application.Current.Shutdown();
            }
            My.log(((Button)sender).Content.ToString(), "Admin");
        }

        public void setParent(navs parenta)
        {
            parent = parenta;
        }

        
        private void Bdelete_Click(object sender, RoutedEventArgs e)
        {
            if (My.authBoss() == false)
                return;
            DataRowView dataRow = (DataRowView)table.SelectedItem;
            if (dataRow == null)
            {
                MessageBox.Show("Please Click the Row to be Deleted", My.shop);
                return;
            }
            string id = dataRow.Row.ItemArray[0].ToString();

            string what = "Are you sure Deleting...\n";
            try
            {
                MessageBoxResult re = MessageBox.Show(what, My.shop, MessageBoxButton.YesNo);
                if (re == MessageBoxResult.Yes)
                {
                    try
                    {
                        string sql = "delete from users where id =" + id;
                        //SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                        //command.ExecuteReader();
                        Mydb.sqlExecutor(sql);
                        MessageBox.Show("Deleted User", My.shop);
                    }
                    catch (Exception eee)
                    {
                        MessageBox.Show("Can't Delete" + eee.Message, My.shop);
                    }
                }
                dt.Rows.Clear();
                initer();
            }
            catch (Exception eee)
            {
                MessageBox.Show("Can't Delete" + eee.Message, My.shop);
            }
            My.log(((Button)sender).Content.ToString(), "Admin");
        }

        private void BnewUser_Click(object sender, RoutedEventArgs e)
        {
            if (My.authAppend() == false)
                return;
            try
            {
                if (p2New.Password == p3New.Password & p2New.Password.Length > 3 & usernameNew.Text != "" & Ctype.Text != "")
                {
                    String sql1 = "insert into users (password,username,type) VALUES ('" + My.MD5Hash(p2New.Password) + "', '" + usernameNew.Text + "', '" + Ctype.Text + "');";
                    //SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
                    //command1.ExecuteNonQuery();
                    Mydb.sqlExecutor(sql1);
                    MessageBox.Show("New User Created", My.shop);

                    usernameNew.Text = "";
                    Ctype.SelectedIndex = -1;
                    initer();

                }
                else
                {
                    MessageBox.Show("Username taken or Passwords Don't Match or need more than 4 characters", My.shop);
                }
            }
            catch (Exception eee)
            {
                MessageBox.Show("Can't Create: " + eee.Message, My.shop);
            }
            My.log(((Button)sender).Content.ToString(), "Admin");
        }

    }
}
