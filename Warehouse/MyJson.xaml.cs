﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for MyJson.xaml
    /// </summary>
    public partial class MyJson : UserControl
    {
        #region Heading DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String Heading
        {
            get { return (String)GetValue(HeadingProperty); }
            set { SetValue(HeadingProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty HeadingProperty =
            DependencyProperty.Register("Heading", typeof(string),
              typeof(MyJson), new PropertyMetadata(""));

        #endregion

        #region Color DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String Color
        {
            get { return (String)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(string),
              typeof(MyJson), new PropertyMetadata(""));

        #endregion

        #region OtherColor DP

        /// <summary>
        /// Gets or sets the Label which is displayed next to the field
        /// </summary>
        public String OtherColor
        {
            get { return (String)GetValue(OtherColorProperty); }
            set { SetValue(OtherColorProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty OtherColorProperty =
            DependencyProperty.Register("OtherColor", typeof(string),
              typeof(MyJson), new PropertyMetadata(""));

        #endregion

        #region FileName DP

        public String FileName
        {
            get { return (String)GetValue(FileNameProperty); }
            set { SetValue(FileNameProperty, value); }
        }

        /// <summary>
        /// Identified the Label dependency property
        /// </summary>
        public static readonly DependencyProperty FileNameProperty =
            DependencyProperty.Register("FileName", typeof(string),
              typeof(MyJson), new PropertyMetadata(""));

        #endregion

        TextBox[] toSave;
        public Dictionary<string, string> json;
        public string[] fields;
        public string[] fieldLabels;

        public MyJson()
        {
            InitializeComponent();
        }

        private void initer()
        {
            toSave = new TextBox[fields.Length];

            canvas.Children.RemoveRange(1, canvas.Children.Count - 1);

            string fromFile = "";
            try
            {
                fromFile = File.ReadAllText(FileName);
            }
            catch { }

            fromFile = fromFile.Replace("\r\n", "").Replace("\t", "").Replace(",}", "}");
            dynamic json = JsonConvert.DeserializeObject(fromFile);
            Dictionary<string, string> myDic = JsonConvert.DeserializeObject<Dictionary<string, string>>(My.s(json));
            int i = 0;

            foreach (string entry in fields)
            {
                Label labelInfo = new Label();
                labelInfo.Content = entry;
                labelInfo.SetValue(Canvas.LeftProperty, double.Parse(15 + ""));
                labelInfo.Width = 220;
                labelInfo.FontSize = 15;
                labelInfo.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                labelInfo.SetValue(Canvas.TopProperty, double.Parse(i * 50 + 10 + ""));


                TextBox text = new TextBox();
                text.Text = tryKey(myDic, entry);
                text.SetValue(Canvas.LeftProperty, double.Parse(255 + ""));
                text.Width = 220;
                text.FontSize = 14;
                text.SetValue(Canvas.TopProperty, double.Parse(i * 50 + 12 + ""));

                Label hint = new Label();
                hint.Content = fieldLabels[i];
                hint.SetValue(Canvas.LeftProperty, double.Parse(255 + ""));
                hint.Width = 260;
                hint.FontSize = 12;
                hint.SetValue(Canvas.TopProperty, double.Parse(i * 50 + 20 + 10 + ""));
                /*
                if (fieldLabels[i].Contains("yes OR no") == true)
                {
                    text.Visibility = Visibility.Hidden;
                    hint.Visibility = Visibility.Hidden;

                    MyToggle yesNo = new MyToggle();
                    yesNo.CheckBox.IsChecked = tryKey(myDic, entry) == "yes" ? true : false;
                    yesNo.CheckBox.Checked += (ss, e) =>
                    {
                        text.Text = "yes";
                    };
                    yesNo.CheckBox.Unchecked += (ss, e) =>
                    {
                        text.Text = "no";
                    };

                    Frame frame = new Frame();
                    frame.SetValue(Canvas.LeftProperty, double.Parse(255 + ""));
                    frame.SetValue(Canvas.TopProperty, double.Parse(i * 50 + 14 + ""));

                    frame.Navigate(yesNo);
                    canvas.Children.Add(frame);
                }*/

                text.TextChanged += (ss, e) =>
                {
                    news.Content = "Please Save";
                };

                toSave[i] = text;
                canvas.Children.Add(labelInfo);
                canvas.Children.Add(hint);
                canvas.Children.Add(text);

                i++;
            }

            canvas.Height = i * 50 + 40;
            canvasRect.Height = i * 50 + 40;

        }

        private string tryKey(Dictionary<string, string> myDic, string e)
        {
            try
            {
                return myDic[e];
            }
            catch { return ""; }
        }

        private void parent_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                initer();
            }
            catch (Exception ee) { }
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string final = "";
            int i = 0;
            foreach (string entry in fields)
            {
                final += ",\"" + entry + "\":\"" + toSave[i++].Text + "\"";
            }
            final += " ";
            File.WriteAllText(FileName, "{" + final.Substring(1) + "}");
            news.Content = "Saved";
            json = null;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            initer();
        }




    }
}
