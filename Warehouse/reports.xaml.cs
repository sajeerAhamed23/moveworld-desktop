﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for reports.xaml
    /// </summary>
    public partial class reports : UserControl
    {
        navs parent;
        // SQLiteConnection m_dbConnection;
        DataTable dt;
        public reports()
        {
            InitializeComponent();

            //m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            //m_dbConnection.Open();

            dt = new DataTable();
            dt.Columns.Add("ID").ReadOnly = true;
            dt.Columns.Add("Driver Name");


            initer();
        }


        public void initer()
        {
            dt.Rows.Clear();
            /*
            string sql = "SELECT * FROM driver";
            dynamic dynJson = Mydb.sqlSelector(sql);


            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {

                    DataRow row = dt.NewRow();

                    row["ID"] = i.id;
                    row["Driver Name"] = i.username;


                    dt.Rows.Add(row);
                }
            }
            table.ItemsSource = dt.DefaultView;
            */
        }


        private void BnewPass_Click(object sender, RoutedEventArgs e)
        {
            My.logScreen("driver");
            Boolean passok = false;
            Boolean userok = false;
            string password = My.MD5Hash(p1.Password);
            string sql = "select password,type from driver WHERE username='" + username.Text + "'";
            dynamic dynJson = Mydb.sqlSelector(sql);


            dt.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    userok = true;
                    DataRow row = dt.NewRow();
                    row["password"] = i.password;
                    String p = row["password"].ToString();
                    if (password == p)
                    {
                        passok = true;
                        if (p2.Password == p3.Password & p2.Password.Length > 3)
                        {
                            String sql1 = "update driver set password ='" + My.MD5Hash(p2.Password) + "' WHERE username='" + username.Text + "';";
                            Mydb.sqlExecutor(sql1);
                            MessageBox.Show("New password set", My.shop);
                        }
                        else
                        {
                            MessageBox.Show("Passwords Don't Match or need more than 4 characters", My.shop);
                        }
                    }
                }

                if (userok)
                {
                    if (!passok)
                    {
                        MessageBox.Show("Current Password Wrong");
                    }
                }
                else
                {
                    MessageBox.Show("Username Error");
                }


            }
            My.log(((Button)sender).Content.ToString(), "Driver");
        }
                
        private void Bdelete_Click(object sender, RoutedEventArgs e)
        {
            //if (My.authBoss() == false)
                //return;
            DataRowView dataRow = (DataRowView)table.SelectedItem;
            if (dataRow == null)
            {
                MessageBox.Show("Please Click the Row to be Deleted", My.shop);
                return;
            }
            string id = dataRow.Row.ItemArray[0].ToString();

            string what = "Are you sure Deleting...\n";
            try
            {
                MessageBoxResult re = MessageBox.Show(what, My.shop, MessageBoxButton.YesNo);
                if (re == MessageBoxResult.Yes)
                {
                    try
                    {
                        string sql = "delete from driver where id =" + id;
                        Mydb.sqlExecutor(sql);
                        MessageBox.Show("Deleted Driver", My.shop);
                    }
                    catch (Exception eee)
                    {
                        MessageBox.Show("Can't Delete" + eee.Message, My.shop);
                    }
                }
                dt.Rows.Clear();
                initer();
            }
            catch (Exception eee)
            {
                MessageBox.Show("Can't Delete" + eee.Message, My.shop);
            }
            My.log(((Button)sender).Content.ToString(), "Driver");
        }

        private void BnewUser_Click(object sender, RoutedEventArgs e)
        {
            if (My.authAppend() == false)
                return;
            My.logScreen("driver");
            try
            {
                if (p2New.Password == p3New.Password & p2New.Password.Length > 3 & usernameNew.Text != "" )
                {
                    String sql1 = "insert into driver (password,username) VALUES ('" + My.MD5Hash(p2New.Password) + "', '" + usernameNew.Text  + "');";
                    Mydb.sqlExecutor(sql1);
                    MessageBox.Show("New User Created", My.shop);

                    usernameNew.Text = "";
                    initer();

                }
                else
                {
                    MessageBox.Show("Driver Name taken or Passwords Don't Match or need more than 4 characters", My.shop);
                }
            }
            catch (Exception eee)
            {
                MessageBox.Show("Can't Create: " + eee.Message, My.shop);
            }
            My.log(((Button)sender).Content.ToString(), "Driver");

        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            //table.Columns[My.colID(dt, "ID")].Width = new DataGridLength(50);
            
            My.fitToNavs(this);
        }

    }
}
