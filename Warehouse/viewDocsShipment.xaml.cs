﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for viewDocsShipment.xaml
    /// </summary>
    public partial class viewDocsShipment : UserControl
    {
        DataTable dt;
        string shipmentOrHbl = "shipment";
        string id;
        string displayName;
        string country;
        Style styleBoldRight = new Style();
        Style styleRight = new Style();
        Style styleBold = new Style();


        public viewDocsShipment()
        {
            InitializeComponent();

            dt = new DataTable();
            dt.Columns.Add("Document Name").ReadOnly = true;
        }

        public void initer(string ida, string displayNamea, string shipmentOrHbla, string countrya)
        {
            shipmentOrHbl = shipmentOrHbla;
            id = ida;
            displayName = displayNamea;
            country = countrya;

            Lnews.Content = "Documents of " + shipmentOrHbl + " - " + displayNamea;

            //changes to My.groupServerAddress and coutry param
            //this is the difference from the module in agent software
            string url = new Mydb.getRequest(My.groupServerAddress+ country + "/calls/alldocsoflaksiri/allfiles.php")
                .Add("which", shipmentOrHbl)
                .Add("who", id)
                .finalize();

            loader.Show();
            Mydb.urlAsync(url, populate);
        }

        private int populate(string txt)
        {
#if POSfliks
            try{
#endif
            dt.Rows.Clear();
            //txt = txt.Replace(" ", "");

            string[] calls = txt.Split("$".ToCharArray());

            for (int i = 0; i < calls.Length; i++)
            {
                DataRow row = dt.NewRow();

                try
                {
                    if (calls[i] == "") continue;
                    row["Document Name"] = calls[i];
                }
                catch { }

                dt.Rows.Add(row);
            }

            table.ItemsSource = dt.DefaultView;

            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }

        private void content_Loaded(object sender, RoutedEventArgs e)
        {
        }


        private void Brec_Click(object sender, RoutedEventArgs e)
        {
            if (table.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a " + shipmentOrHbl, My.shop);
                return;
            }
            else
            {
                DataRowView dataRow = (DataRowView)table.SelectedItem;
                string filename = dataRow.Row.ItemArray[My.colID(dt, "Document Name")].ToString();

                if (filename == "")
                    return;
                string uri = My.rawServerAddress + "calls/alldocsoflaksiri/" + shipmentOrHbl + "/" + id + "/" + filename;

                using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                {
                    try
                    {
                        dialog.SelectedPath = File.ReadAllText("Params/defaultPath.json"); ;
                    }
                    catch { }
                    System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        string location = dialog.SelectedPath;
                        try
                        {
                            File.WriteAllText("Params/defaultPath.json", location);
                        }
                        catch { }
                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile(uri.Replace(" ", "%20"), location + "/" + filename);
                            //MessageBox.Show("File downloaded to location specified",My.shop);
                            MessageBoxResult re = MessageBox.Show("File downloaded. Would you like to open?", My.shop, MessageBoxButton.OKCancel);
                            if (re == MessageBoxResult.OK)
                            {
                                Process.Start(location + "/" + filename);
                            }
                        }
                    }
                }

            }
        }

        private void Label_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void Bdelete_Click(object sender, RoutedEventArgs e)
        {
            if (table.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a " + shipmentOrHbl, My.shop);
                return;
            }
            else
            {
                MessageBoxResult re = MessageBox.Show("Are you sure to Delete", My.shop, MessageBoxButton.YesNo);
                if (re == MessageBoxResult.Yes)
                {
                    DataRowView dataRow = (DataRowView)table.SelectedItem;
                    string filename = dataRow.Row.ItemArray[My.colID(dt, "Document Name")].ToString();

                    if (filename == "")
                        return;

                    string url = My.groupServerAddress + country + "/calls/alldocsoflaksiri/delete.php?path=" + shipmentOrHbl + "/" + id + "/&name=" + filename;
                    Mydb.fetchUrl(url);
                    initer(id, displayName, shipmentOrHbl,country);
                }
            }
        }




    }
}
