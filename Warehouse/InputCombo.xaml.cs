﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for InputCombo.xaml
    /// </summary>
    public partial class InputCombo : Window
    {
        ArrayList ID; 
        public InputCombo(string question, ArrayList defaultAnswer, ArrayList IDs )
        {
            InitializeComponent();
            lblQuestion.Content = question;
            txtAnswer.ItemsSource= defaultAnswer;
            this.Topmost = true;
            ID = IDs;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            if (txtAnswer.SelectedIndex!=-1)
                this.DialogResult = true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            txtAnswer.Focus();
            this.Topmost = true;
        }

        public object Answer
        {

            get {
                return ID[txtAnswer.SelectedIndex]; }
        }

    }
}