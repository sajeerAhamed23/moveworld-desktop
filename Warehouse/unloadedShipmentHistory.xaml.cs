﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for unloadedShipmentHistory.xaml
    /// </summary>
    public partial class unloadedShipmentHistory : UserControl
    {
        DataTable dtMain, dtDown;
        public unloadedShipmentHistory()
        {
            InitializeComponent();



            dtMain = new DataTable();
            dtMain.Columns.Add("ID").ReadOnly = true;
            dtMain.Columns.Add("Name").ReadOnly = true;
            dtMain.Columns.Add("Ref").ReadOnly = true;
            dtMain.Columns.Add("Country").ReadOnly = true;
            dtMain.Columns.Add("Cargo Type").ReadOnly = true;
            dtMain.Columns.Add("BL Number");
            dtMain.Columns.Add("Created Time").ReadOnly = true;
            dtMain.Columns.Add("Created Date").ReadOnly = true;
            //dtMain.Columns.Add("Has Reached").ReadOnly = true;
            dtMain.Columns.Add("ETA");



            dtDown = new DataTable();
            dtDown.Columns.Add("HBL").ReadOnly = true;
            dtDown.Columns.Add("Type").ReadOnly = true;
            dtDown.Columns.Add("Volume").ReadOnly = true;
            dtDown.Columns.Add("Quantity").ReadOnly = true;
            dtDown.Columns.Add("Weight").ReadOnly = true;
            dtDown.Columns.Add("Name").ReadOnly = true;
            dtDown.Columns.Add("PID").ReadOnly = true;



            datestart1.SelectedDate = DateTime.Now.AddMonths(-2);
            dateend.SelectedDate = DateTime.Now.AddDays(1);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            My.fitToNavs(this);
        }


        private void Bsubmit_Click_1(object sender, RoutedEventArgs e)
        {
            initer();
        }

        public void initer()
        {
            string tds = DateTime.Parse(datestart1.Text).ToString("yyyy-MM-dd");
            string tde = DateTime.Parse(dateend.Text).ToString("yyyy-MM-dd");

            string sql = "SELECT *,ref as refe FROM loading WHERE is_deleted=0 AND fully_unloaded=1 AND date_>='" + tds + "' AND date_<='" + tde + "'";

            loader.Show();
            Mydb.sqlSelectorAsync(sql, populate);
        }

        String country = "";
        private void table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView dataRow = (DataRowView)table.SelectedItem;
                string id = dataRow.Row.ItemArray[My.colID(dtMain, "Ref")].ToString();
                country = dataRow.Row.ItemArray[My.colID(dtMain, "Country")].ToString();

                initerDown(id);
            }
            catch (Exception es) { }
        }

        private int populate(dynamic dynJson)
        {
#if POSfliks
            try{
#endif
            dtMain.Rows.Clear();
            dtDown.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    DataRow row = dtMain.NewRow();
                    row["ID"] = i.id;
                    row["Ref"] = i.refe;
                    row["Country"] = i.country;
                    row["Name"] = i.notes;
                    row["Cargo Type"] = My.s(i.isAir) == "1" ? "Air Cargo" : "Sea Cargo";
                    //row["Has Reached"] = My.s(i.reached) == "1" ? "Yes" : "No";
                    row["Created Time"] = i.time_;
                    row["Created Date"] = i.date_;
                    row["ETA"] = i.reached_date;
                    row["BL Number"] = i.bl_number;

                    dtMain.Rows.Add(row);
                }
            }

            table.ItemsSource = dtMain.DefaultView;

            try
            {

                table.Columns[My.colID(dtMain, "ID")].Width = new DataGridLength(30);
            }
            catch (Exception eer) { }

            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }


        private void initerDown(string loading_id)
        {

            string sql = "SELECT cargo.id as idh,package.id as idp,pickedup_date, package.type as typePack, name, (length * breadth * heigth * quantity) as volume, (quantity) as quantity_tot, (weigth) as weight_tot, pickup_date FROM cargo,package where cargo.id = package.cargo_id and loading_id =" + loading_id;

            loader.Show();
            Mydbs.sqlSelectorAsync(country, sql, populateDown);
        }

        private int populateDown(dynamic dynJson)
        {
#if POSfliks
            try{
#endif
            dtDown.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    DataRow row = dtDown.NewRow();

                    row["HBL"] = i.idh;
                    row["PID"] = i.idp;
                    row["Type"] = i.typePack;
                    row["Volume"] = i.volume;
                    row["Quantity"] = i.quantity;
                    row["Weight"] = i.weight_tot;
                    row["Name"] = i.name;

                    dtDown.Rows.Add(row);
                }
            }

            tableDown.ItemsSource = dtDown.DefaultView;

            try
            {
                tableDown.Columns[My.colID(dtDown, "HBL")].Width = new DataGridLength(30);
            }
            catch (Exception eer) { }
            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }
    }
}
