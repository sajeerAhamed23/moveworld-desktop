﻿using Newtonsoft.Json;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for viewFetch.xaml
    /// </summary>
    public partial class warehouse : UserControl
    {

        DataTable dt;
        public warehouse()

        {
            InitializeComponent();

            dt = new DataTable();
            dt.Columns.Add("HBL").ReadOnly = true;
            dt.Columns.Add("PID").ReadOnly = true;
            dt.Columns.Add("Loading ID").ReadOnly = true;
            dt.Columns.Add("Country").ReadOnly = true;
            dt.Columns.Add("Zone").ReadOnly = true;
            dt.Columns.Add("Notes").ReadOnly = true;
        }

        private void page_Loaded(object sender, RoutedEventArgs e)
        {
            My.fitToNavs(this);
        }


        private void Bsubmit_Click(object sender, RoutedEventArgs e)
        {
            initer();
        }

        private void initer()
        {
            // string sql = "SELECT cargo.id as idh,package.id as idp,pickedup_date, package.type as typePack, name, (length * breadth * heigth * quantity) as volume, (quantity) as quantity_tot, (weigth) as weight_tot, pickup_date FROM cargo,package where cargo.id = package.cargo_id and loading_id =" + loading_id;

            string sql = "select * from package";
            loader.Show();
            Mydb.sqlSelectorAsync( sql, populateDown);
        }

        private int populateDown(dynamic dynJson)
        {
#if POSfliks
            try{
#endif
            dt.Rows.Clear();
            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    DataRow row = dt.NewRow();

                    row["HBL"] = i.hbl_id;
                    row["PID"] = i.package_id;
                    row["Loading ID"] = i.loading_id;
                    row["Country"] = i.country;
                    row["Zone"] = i.zone;
                    row["Notes"] = i.notes;

                    dt.Rows.Add(row);
                }
            }

            table.ItemsSource = dt.DefaultView;

            try
            {
                table.Columns[My.colID(dt, "HBL")].Width = new DataGridLength(30);
            }
            catch (Exception eer) { }
            loader.Hide();
#if POSfliks
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif
            return 1;
        }
    }
}