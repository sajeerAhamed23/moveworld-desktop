﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Warehouse
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {
                string logFolder = System.IO.Path.Combine(Environment.CurrentDirectory, "Logs/" + DateTime.Now.ToString("yyyy MM dd"));

                My.logFileName = logFolder + " Log.txt";
                My.errorFileName = logFolder + " Err.txt";
            }
            catch { }

            string logFile = "Log\\Log " + DateTime.Now.ToString("yyyy MM dd HH mm ss") + ".txt";
            My.setLogFileName(logFile);

            My.rawServerAddress = My.groupServerAddress + My.settingsValue("Branch") + "/";
            My.FTPusername = My.settingsValue("Branch") + "xyz@laksiri.cf";
            My.serverAddress = My.rawServerAddress + "api/"; 
            try
            {
                My.shop = My.settingsValue("name");
            }
            catch (Exception d) { My.shop = ""; }

            
#if prod6
            try{
#endif

#if prod6
            }catch (Exception bizee){My.handleException(bizee, System.Reflection.MethodBase.GetCurrentMethod().Name, this);}
#endif

            Process[] pname = Process.GetProcessesByName(AppDomain.CurrentDomain.FriendlyName.Remove(AppDomain.CurrentDomain.FriendlyName.Length - 4));
            if (pname.Length > 1)
            {
                MessageBoxResult re = MessageBox.Show("Already running! You want to close it?", "TechFliks 1.0", MessageBoxButton.YesNo);
                if (re == MessageBoxResult.Yes)
                {
                    pname.Where(p => p.Id != Process.GetCurrentProcess().Id).First().Kill();
                }
                else
                    Application.Current.Shutdown();
            }

            My.fingerPrint = MyFingerPrint.Value();

            isValidClient();
            //My.test();

            try
            {
                username.Text = My.settingsValue("default_username");
            }
            catch (Exception d) { username.Text = ""; }


#if prod6

#else
            My.type = My.s("Admin");
            My.user = "Arshad";
            new navs().Show();
            this.Hide();
#endif

        }

        private void isValidClient()
        {
            try
            {
                string line;
                using (var reader = new StreamReader(@"C:\\Windows\\fliks.key"))
                {
                    line = reader.ReadLine();
                }
            }
            catch (Exception eer)
            {
                MessageBox.Show("Invalid Client", "Techfliks");
                Application.Current.Shutdown();
            }
        }

        private void AllowUIToUpdate()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Render, new DispatcherOperationCallback(delegate(object parameter)
            {
                frame.Continue = false;
                return null;
            }), null);
            Dispatcher.PushFrame(frame);
        }

        private void Blogin_Click(object sender, RoutedEventArgs e)
        {
            checkPassword();

        }

        private void checkPassword()
        {
            //new navs().Show();
            //this.Hide();
            double xx = SystemParameters.VirtualScreenWidth;
            double yy = SystemParameters.VirtualScreenHeight;
            /*
             * MAC Check
             var macAddr = 
    (
        from nic in NetworkInterface.GetAllNetworkInterfaces()
        where nic.OperationalStatus == OperationalStatus.Up
        select nic.GetPhysicalAddress().ToString()
    ).FirstOrDefault();
             
             */

            labelPass.Content = "Please Wait... Loading...";
            AllowUIToUpdate();

            Boolean passok = false;
            Boolean userok = false;
            string password = My.MD5Hash(pass.Password);
            string sql = "select username,password,type from users WHERE username='" + username.Text + "'";
            dynamic dynJson = Mydb.sqlSelector(sql);

            if (dynJson != null)
            {
                foreach (var i in dynJson)
                {
                    userok = true;
                    string p = My.s(i.password);
                    if (password == p)
                    {
                        passok = true;
                        My.type = My.s(i.type);
                        My.user = My.s(i.username);
                    }
                }
            }
            if (userok)
            {
                if (passok)
                {
                    CommandManager.InvalidateRequerySuggested();
                    new navs(username.Text).Show();
                    this.Hide();
                }
                else
                {
                    labelPass.Content = "Password Error";
                    labelPass.FontSize = 14;
                    pass.Focus();
                }
            }
            else
            {
                labelPass.Content = "Username Error";
                labelPass.FontSize = 14;
                username.Focus();
            }
        }



        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();

        }

        private void x(object sender, EventArgs e)
        {
            // loginButoon.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            username.Focus();
        }

        private void username_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                pass.Focus();
            }
        }

        private void pass_KeyDown(object sender, KeyEventArgs e)
        {
            labelPass.Content = "";
            if (e.Key == Key.Return)
            {
                labelPass.Content = "Please Wait... Loading...";
                AllowUIToUpdate();
                checkPassword();
                //loginButoon.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
            if (e.Key == Key.Escape)
            {
                cancel.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
        }

        private void pass_TextInput(object sender, TextCompositionEventArgs e)
        {
            labelPass.Content = "";
        }



        private void ds(object sender, RoutedEventArgs e)
        {
            // n.Position = new TimeSpan(0,0,1);
            // n.Play();
        }

        private void Bforgot_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Please contact\r\nTechfliks Developers for New Password", My.shop);

        }

        private void Bsite_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.techfliks.com/");
        }

        private void type_pass(object sender, RoutedEventArgs e)
        {
            pass.Password = pass.Password + (sender as Button).Content.ToString();
            if (pass.Password.Length == 4)
                Blogin_Click(sender, e);
        }

        private void type_pass_reset(object sender, RoutedEventArgs e)
        {
            pass.Password = "";
        }


    }
}
